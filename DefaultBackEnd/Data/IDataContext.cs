﻿namespace Data
{
    public interface IDataContext
    {
        IEntityContext<User> UserContext { get; }

        IEntityContext<Role> RoleContext { get; }
    }
}