﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Data
{
    public class RoleEntityContext : IEntityContext<Role>
    {
        private int idPoint;
        private readonly ConcurrentDictionary<int, Role> roles = new ConcurrentDictionary<int, Role>();

        public Role Create(Role role)
        {
            var result = new Role
            {
                Name = role.Name,
                Id = ++idPoint
            };

            roles.TryAdd(idPoint, result);

            return result;
        }

        public bool Delete(int id)
        {
            var role = Find(id);

            var result = false;

            if (role != null) result = roles.TryRemove(id, out role);

            return result;
        }

        public bool Edit(int id, Role editedRole)
        {
            var role = Find(editedRole.Id);

            var result = false;

            if (role != null)
            {
                role.Name = editedRole.Name;

                result = true;
            }

            return result;
        }

        public ICollection<Role> Get()
        {
            var result = roles.Values;

            return result;
        }

        private Role Find(int id)
        {
            var result = Get().FirstOrDefault(x => x.Id == id);

            return result;
        }
    }
}