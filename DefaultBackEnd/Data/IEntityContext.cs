﻿using System.Collections.Generic;

namespace Data
{
    public interface IEntityContext<T>
        where T : IEntity
    {
        T Create(T entity);
        bool Edit(int id, T entity);
        ICollection<T> Get();
        bool Delete(int id);
    }
}