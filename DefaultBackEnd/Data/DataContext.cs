﻿namespace Data
{
    public class DataContext : IDataContext
    {
        public DataContext()
        {
            UserContext = new UserEntityContext();
            RoleContext = new RoleEntityContext();
        }

        public IEntityContext<User> UserContext { get; }

        public IEntityContext<Role> RoleContext { get; }
    }
}