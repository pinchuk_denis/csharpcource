﻿using Domain.Role;
using Domain.Role.FromDb.Create;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Api.Areas.Entities.FromDb
{
    [Area("Entities/FromDb")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IMediator mediator;

        public RolesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        ///     Create role
        /// </summary>
        /// <response code="201">Role created</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<Response> CreateRole([FromBody] Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Role details
        /// </summary>
        /// <response code="201">Role details</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromDb.Details.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet("roleId")]
        public async Task<Domain.Role.FromDb.Details.Response> DetailsRole(int roleId)
        {
            var result = await mediator.Send(new Domain.Role.FromDb.Details.Request(new RoleKey(roleId)));

            return result;
        }

        /// <summary>
        ///     Delete role
        /// </summary>
        /// <response code="201">Role deleted</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromDb.Delete.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpDelete("roleId")]
        public async Task<Domain.Role.FromDb.Delete.Response> DeleteRole(int roleId)
        {
            var result = await mediator.Send(new Domain.Role.FromDb.Delete.Request(new RoleKey(roleId)));

            return result;
        }

        /// <summary>
        ///     Get role list
        /// </summary>
        /// <response code="201">Role list</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromDb.List.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<Domain.Role.FromDb.List.Response> UserList([FromQuery] Domain.Role.FromDb.List.Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Edit role
        /// </summary>
        /// <response code="201">Role edited</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromDb.Edit.Response), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [HttpPut("roleId")]
        public async Task<Domain.Role.FromDb.Edit.Response> EditUser(int roleId, [FromBody] Domain.Role.FromDb.Edit.Request request)
        {
            request.RoleKey = new RoleKey(roleId);

            var result = await mediator.Send(request);

            return result;
        }
    }
}