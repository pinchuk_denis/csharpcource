﻿using Api.Authorization;
using Domain.User;
using Domain.User.FromDb.Create;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Api.Areas.Entities.FromDb
{
    [Area("Entities/FromDb")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator mediator;

        public UsersController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        ///     Create user
        /// </summary>
        /// <response code="201">User created</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpPost]
        [AuthorizeByApiKey]
        public async Task<Response> CreateUser([FromBody] Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     User details
        /// </summary>
        /// <response code="201">User details</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromDb.Details.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet("userId")]
        [AuthorizeByApiKey]
        public async Task<Domain.User.FromDb.Details.Response> DetailsUser(int userId)
        {
            var result = await mediator.Send(new Domain.User.FromDb.Details.Request(new UserKey(userId)));

            return result;
        }

        /// <summary>
        ///     Delete user
        /// </summary>
        /// <response code="201">User deleted</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromDb.Delete.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpDelete("userId")]
        [AuthorizeByApiKey]
        public async Task<Domain.User.FromDb.Delete.Response> DeleteUser(int userId)
        {
            var result = await mediator.Send(new Domain.User.FromDb.Delete.Request(new UserKey(userId)));

            return result;
        }

        /// <summary>
        ///     Get user list
        /// </summary>
        /// <response code="201">User list</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromDb.List.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet]
        [AuthorizeByApiKey]
        public async Task<Domain.User.FromDb.List.Response> UserList([FromQuery] Domain.User.FromDb.List.Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Edit user
        /// </summary>
        /// <response code="201">User edited</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromDb.Edit.Response), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [HttpPut("userId")]
        [AuthorizeByApiKey]
        public async Task<Domain.User.FromDb.Edit.Response> EditUser(int userId, [FromBody] Domain.User.FromDb.Edit.Request request)
        {
            request.UserKey = new UserKey(userId);

            var result = await mediator.Send(request);

            return result;
        }
    }
}