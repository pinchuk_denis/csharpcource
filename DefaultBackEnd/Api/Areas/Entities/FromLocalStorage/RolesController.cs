﻿using Domain.Role;
using Domain.Role.FromLocalStorage.Create;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Api.Areas.Entities.FromLocalStorage
{
    [Area("Entities/FromLocalStorage")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IMediator mediator;

        public RolesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        ///     Create role
        /// </summary>
        /// <response code="201">Role created</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<Response> CreateRole([FromBody] Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Role details
        /// </summary>
        /// <response code="201">Role details</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromLocalStorage.Details.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet("roleId")]
        public async Task<Domain.Role.FromLocalStorage.Details.Response> DetailsRole(int roleId)
        {
            var result = await mediator.Send(new Domain.Role.FromLocalStorage.Details.Request(new RoleKey(roleId)));

            return result;
        }

        /// <summary>
        ///     Delete role
        /// </summary>
        /// <response code="201">Role deleted</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromLocalStorage.Delete.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpDelete("roleId")]
        public async Task<Domain.Role.FromLocalStorage.Delete.Response> DeleteRole(int roleId)
        {
            var result = await mediator.Send(new Domain.Role.FromLocalStorage.Delete.Request(new RoleKey(roleId)));

            return result;
        }

        /// <summary>
        ///     Get role list
        /// </summary>
        /// <response code="201">Role list</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromLocalStorage.List.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<Domain.Role.FromLocalStorage.List.Response> UserList([FromQuery] Domain.Role.FromLocalStorage.List.Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Edit role
        /// </summary>
        /// <response code="201">Role edited</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.Role.FromLocalStorage.Edit.Response), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int)HttpStatusCode.BadRequest)]
        [HttpPut("roleId")]
        public async Task<Domain.Role.FromLocalStorage.Edit.Response> EditUser(int roleId, [FromBody] Domain.Role.FromLocalStorage.Edit.Request request)
        {
            request.RoleKey = new RoleKey(roleId);

            var result = await mediator.Send(request);

            return result;
        }
    }
}