﻿using Domain.User;
using Domain.User.FromLocalStorage.Create;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Api.Areas.FromLocalStorage.Entities
{
    [Area("Entities")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMediator mediator;

        public UsersController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        ///     Create user
        /// </summary>
        /// <response code="201">User created</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpPost]
        public async Task<Response> CreateUser([FromBody] Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     Edit user
        /// </summary>
        /// <response code="201">User edited</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromLocalStorage.Edit.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpPut("userId")]
        public async Task<Domain.User.FromLocalStorage.Edit.Response> EditUser(int userId, [FromBody] Domain.User.FromLocalStorage.Edit.Request request)
        {
            request.UserKey = new UserKey(userId);

            var result = await mediator.Send(request);

            return result;
        }

        /// <summary>
        ///     User details
        /// </summary>
        /// <response code="201">User details</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromLocalStorage.Details.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet("userId")]
        public async Task<Domain.User.FromLocalStorage.Details.Response> DetailsUser(int userId)
        {
            var result = await mediator.Send(new Domain.User.FromLocalStorage.Details.Request(new UserKey(userId)));

            return result;
        }

        /// <summary>
        ///     Delete user
        /// </summary>
        /// <response code="201">User deleted</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromLocalStorage.Delete.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpDelete("userId")]
        public async Task<Domain.User.FromLocalStorage.Delete.Response> DeleteUser(int userId)
        {
            var result = await mediator.Send(new Domain.User.FromLocalStorage.Delete.Request(new UserKey(userId)));

            return result;
        }

        /// <summary>
        ///     Get user list
        /// </summary>
        /// <response code="201">User list</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Domain.User.FromLocalStorage.List.Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet]
        public async Task<Domain.User.FromLocalStorage.List.Response> UserList([FromQuery] Domain.User.FromLocalStorage.List.Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }
    }
}