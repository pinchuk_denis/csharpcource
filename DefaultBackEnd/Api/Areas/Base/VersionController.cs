﻿using Domain.Version.Get;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace Api.Areas.Base
{
    [Area("Default")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        private readonly IMediator mediator;

        public VersionController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        /// <summary>
        ///     Gett version
        /// </summary>
        /// <response code="201">Version</response>
        /// <response code="400">Request has incorrect data</response>
        [ProducesResponseType(typeof(Response), (int) HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ErrorResponse), (int) HttpStatusCode.BadRequest)]
        [HttpGet("Status")]
        public async Task<Response> Status([FromQuery] Request request)
        {
            var result = await mediator.Send(request);

            return result;
        }
    }
}