﻿using Infrastructure;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.Authorization
{
    public class AuthorizeByApiKeyFilter : IAuthorizationFilter, IAuthorizeByApiKeyFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
        }
    }
}
