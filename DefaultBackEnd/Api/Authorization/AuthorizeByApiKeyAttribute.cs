﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Authorization
{
    public class AuthorizeByApiKeyAttribute : TypeFilterAttribute
    {
        public AuthorizeByApiKeyAttribute() : base(typeof(AuthorizeByApiKeyFilter))
        {
        }
    }
}
