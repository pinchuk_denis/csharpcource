﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace Api.Authorization
{
    public class ApiKeyValidationMiddleware
    {
        private readonly RequestDelegate next;

        public ApiKeyValidationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            await ValidateApiKeyAsync(context);

            await this.next.Invoke(context);
        }

        private static async Task ValidateApiKeyAsync(HttpContext context)
        {
            var mediator = context.RequestServices.GetRequiredService<IMediator>();

            await mediator.Send(new Domain.Authorization.ApiKey.Validate.Request());
        }
    }
}
