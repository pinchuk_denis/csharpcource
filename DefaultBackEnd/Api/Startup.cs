using Api.config;
using Infrastructure;
using Infrastructure.Sorting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore().AddApiExplorer();

            var scanAssembliesOfTypes = new[]
                {
                    typeof(Startup),
                    typeof(Domain.Startup),
                    typeof(Infrastructure.Startup)
                }
                .ToArray();

            services.Scan(scan => scan
                .FromAssembliesOf(scanAssembliesOfTypes)
                .AddClasses(classes => classes.AssignableTo<ISingletonProvider>())
                .AsSelf()
                .WithSingletonLifetime());

            services.Scan(scan => scan
                .FromAssembliesOf(scanAssembliesOfTypes)
                .AddClasses(classes => classes.AssignableTo<ISortingRules>())
                .AsSelf()
                .WithScopedLifetime());

            DataContextConfig.RegisterDataContextServices(services, Configuration);

            SwaggerConfig.RegisterSwaggerGenServices(services);

            LocalizationConfig.RegisterLocalizationServices(services);

            HttpConfig.RegisterServices(services);

            ApiKeyConfig.RegisterServices(services, this.Configuration);

            ErrorHandlerConfig.RegisterServices(services);

            MvcConfig.RegisterMvcServices(services);

            CqrsConfig.RegisterCqrsServices(services);

            FluentValidationConfig.RegisterFluentValidationServices(services);

            DataContextConfig.RegisterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipel.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            SwaggerConfig.UseSwagger(app);

            ErrorHandlerConfig.UseErrorHandler(app);

            HttpConfig.UseHttps(app);

            ApiKeyConfig.UseApiKeyValidation(app);

            app.UseRouting();

            MvcConfig.UseEndpoints(app);

            FluentValidationConfig.ConfigureFluentValidationUsage();
        }
    }
}