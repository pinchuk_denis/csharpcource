﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Api.config
{
    public static class FluentValidationConfig
    {
        public static void ConfigureFluentValidationUsage()
        {
            ValidatorOptions.Global.CascadeMode = CascadeMode.Stop;
        }

        public static void RegisterFluentValidationServices(IServiceCollection services)
        {
            var scanAssembliesOfTypes = new[]
                {
                    typeof(Startup),
                    typeof(Domain.Startup),
                    typeof(Infrastructure.Startup)
                }
                .ToArray();

            services.Scan(scan => scan
                .FromAssembliesOf(scanAssembliesOfTypes)
                .AddClasses(classes => classes.AssignableTo<IValidator>())
                .AsSelf()
                .AsImplementedInterfaces()
                .WithScopedLifetime());
        }
    }
}