﻿using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Api.config
{
    public static class CqrsConfig
    {
        public static void RegisterCqrsServices(IServiceCollection services)
        {
            var scanAssembliesOfTypes = new[]
                {
                    typeof(Startup),
                    typeof(Domain.Startup),
                    typeof(Infrastructure.Startup)
                }
                .ToArray();

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));
            services.AddMediatR(scanAssembliesOfTypes);
        }
    }
}