﻿using Data;
using DataDb.Entities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api.config
{
    public static class DataContextConfig
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddSingleton<IDataContext, DataContext>();
        }

        public static void RegisterDataContextServices(
            IServiceCollection services,
            IConfiguration configuration)
        {
            var databaseOptions = new DatabaseOptions
            {
                ConnectionString =
                    configuration.GetRequiredValue<string>("DBEDatabase:ConnectionStrings:MsSqlDb"),
                CommandTimeoutSeconds = 30
            };

            RegisterSqlDataContextServices(services, databaseOptions);
        }

        public static void RegisterSqlDataContextServices(
            IServiceCollection services,
            DatabaseOptions databaseOptions)
        {
            services.AddDbContext<DataContextDb>(
                dbContextOptionsBuilder => { ConfigureDataContextOptions(dbContextOptionsBuilder, databaseOptions); });
        }

        public static void ConfigureDataContextOptions(DbContextOptionsBuilder dbContextOptionsBuilder,
            DatabaseOptions databaseOptions)
        {
            dbContextOptionsBuilder.UseSqlServer(
                databaseOptions.ConnectionString,
                sqlServerOptionsAction => sqlServerOptionsAction.CommandTimeout(databaseOptions.CommandTimeoutSeconds));

            // Throw exception to prevent EF executes query locally
            dbContextOptionsBuilder.ConfigureWarnings(x => x.Default(WarningBehavior.Throw));

            dbContextOptionsBuilder.ConfigureWarnings(w => w.Ignore(CoreEventId.ManyServiceProvidersCreatedWarning));

            dbContextOptionsBuilder.ConfigureWarnings(w =>
                w.Ignore(CoreEventId.PossibleUnintendedCollectionNavigationNullComparisonWarning));
        }
    }
}