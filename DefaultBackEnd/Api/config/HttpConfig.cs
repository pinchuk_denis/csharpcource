﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.DependencyInjection;

namespace Api.config
{
    public static class HttpConfig
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.Configure<MvcOptions>(options => { options.Filters.Add(new RequireHttpsAttribute()); });

            services.AddHttpContextAccessor();

            services.AddHttpClient();
        }

        public static void UseHttps(IApplicationBuilder app)
        {
            var options = new RewriteOptions().AddRedirectToHttps();

            app.UseRewriter(options);
        }
    }
}