﻿using Api.Authorization;
using Domain.Authorization.ApiKey;
using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Api.config
{
    public static class ApiKeyConfig
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ApiKeyAuthorizationOptions>(options =>
            {
                options.ApiKey = configuration.GetRequiredValue<string>("Application:ApiKey");
            });
        }

        public static void UseApiKeyValidation(IApplicationBuilder app)
        {
            app.UseMiddleware<ApiKeyValidationMiddleware>();
        }
    }
}
