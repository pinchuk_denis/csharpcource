﻿using Infrastructure.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.IO;

namespace Api.config
{
    public static class SwaggerConfig
    {
        public static void UseSwagger(IApplicationBuilder app)
        {
            app.UseSwagger(options => { options.RouteTemplate = "{documentName}/swagger.json"; });

            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = "swagger";
                options.SwaggerEndpoint("/v1/swagger.json", "Base API");
                options.DocExpansion(DocExpansion.None);
            });
        }

        public static void RegisterSwaggerGenServices(IServiceCollection services)
        {
            string basePath = AppContext.BaseDirectory;
            var xmlDocsFilePath = Path.Combine(basePath, "Base.Api.xml");

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo {Title = "Base API", Version = "v1"});
                options.IncludeXmlComments(xmlDocsFilePath);
                options.OperationFilter<JsonIgnoreQueryOperationFilter>();
                options.SchemaFilter<EnumSchemaFilter>();
                options.SchemaFilter<IgnoreReadOnlySchemaFilter>();
                options.OperationFilter<ApiKeyAuthorizationHeaderParameterOperationFilter>();

                var securitySchema = new OpenApiSecurityScheme
                {
                    Description =
                        "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                };

                options.AddSecurityDefinition("Bearer", securitySchema);

                var securityRequirement = new OpenApiSecurityRequirement
                {
                    {securitySchema, new[] {"Bearer"}}
                };

                options.AddSecurityRequirement(securityRequirement);
            });

            services.ConfigureSwaggerGen(options => { options.CustomSchemaIds(x => x.FullName); });
        }
    }
}