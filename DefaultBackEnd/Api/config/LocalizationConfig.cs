﻿using Infrastructure.Localization;
using Infrastructure.Localization.Enum;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace Api.config
{
    public static class LocalizationConfig
    {
        public static void UseRequestLocalization(IApplicationBuilder app)
        {
            var supportedCultures = new[]
            {
                new CultureInfo("en-US")
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
        }

        public static void RegisterLocalizationServices(IServiceCollection services)
        {
            services.AddLocalization();

            services.AddScoped<ResourceStringLocalizer>();

            services.AddScoped<EnumLocalizer>();

            services.AddScoped<EnumLocalizedAvailableValuesProvider>();
        }
    }
}