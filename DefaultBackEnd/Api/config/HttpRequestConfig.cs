﻿namespace Api.config
{
    public static class HttpRequestConfig
    {
        public const int RequestSizeLimitBytes = 100 * 1024 * 1024; // 100MB
    }
}