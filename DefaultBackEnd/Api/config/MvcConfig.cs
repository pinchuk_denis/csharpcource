﻿using Infrastructure;
using Infrastructure.ModelBinding;
using Infrastructure.RouteConstraint;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Api.config
{
    public static class MvcConfig
    {
        public static IMvcBuilder RegisterMvcServices(IServiceCollection services)
        {
            var mvcBuilder = SharedRegisterMvcServices(services,
                options => { });

            services.Configure<RouteOptions>(options =>
                options.ConstraintMap.Add("enum", typeof(EnumConstraint)));

            services.AddControllers();

            return mvcBuilder;
        }

        [Obsolete]
        public static IMvcBuilder SharedRegisterMvcServices(IServiceCollection services,
            Action<MvcOptions> setupAction = null)
        {
#pragma warning disable ASP5001 // Type or member is obsolete
            var mvcBuilder =
                services
                    .AddMvc(options =>
                    {
                        options.Filters.Add(typeof(FormatModelBindingErrorsFilter));
                        options.RespectBrowserAcceptHeader = true;
                        options.EnableEndpointRouting = false;
                        setupAction?.Invoke(options);
                    })
                    .AddNewtonsoftJson(options =>
                    {
                        options.SerializerSettings.ContractResolver =
                            JsonSerializerSettingsProvider.GetDefaultContractResolver();
                        options.SerializerSettings.Converters.Add(JsonSerializerSettingsProvider.GetDefaultConverter());
                        options.SerializerSettings.ReferenceLoopHandling =
                            JsonSerializerSettingsProvider.GetDefaultReferenceLoopHandling();
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Latest);
#pragma warning restore ASP5001 // Type or member is obsolete

            return mvcBuilder;
        }

        public static void UseEndpoints(IApplicationBuilder app)
        {
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}