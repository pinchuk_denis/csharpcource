﻿using Infrastructure.Exceptions;
using Infrastructure.HttpResponse;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Api.config
{
    public class ErrorHandlerConfig
    {
        public static void UseErrorHandler(IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }

        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IExceptionResponseHandler, ExceptionResponseHandler>();
            services.AddScoped<ValidationExceptionHandler>();
            services.AddScoped<HttpResponseWriter>();
            services.AddScoped<BusinessExceptionHandler>();
            services.AddScoped<ValidationExceptionHandler>();
            services.AddScoped<UnhandledExceptionHandler>();
            services.AddScoped<ModelBindingExceptionHandler>();
            services.AddScoped<HttpResponseWriter>();
            services.AddScoped<DbUpdateConcurrencyExceptionHandler>();
        }
    }
}