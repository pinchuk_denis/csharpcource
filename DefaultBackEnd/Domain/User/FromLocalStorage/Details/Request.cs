﻿using Infrastructure.Cqrs.Queries;

namespace Domain.User.FromLocalStorage.Details
{
    public class Request : IQuery<Response>
    {
        public Request(UserKey userKey)
        {
            UserKey = userKey;
        }

        public UserKey UserKey { get; }
    }
}