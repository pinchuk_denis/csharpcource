﻿using Infrastructure.Response;

namespace Domain.User.FromLocalStorage.Details
{
    public class Response : TokenedDataResponse<Data.User>
    {
        public Response(Data.User user)
            : base(user)
        {
        }
    }
}