﻿using Data;
using Infrastructure.Cqrs.Queries;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Details
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsUserExistsAsync(request);

            var user = GetUserDetails(request);

            return new Response(user);
        }

        private Data.User GetUserDetails(Request request)
        {
            return dataContext.UserContext.Get().FirstOrDefault(x => x.Id == request.UserKey.UserId);
        }

        private async Task EnsureIsUserExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.UserKey));
        }
    }
}