﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Delete
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsUserExistsAsync(request);

            bool isSucceeded = DeleteUser(request);

            return new Response(isSucceeded);
        }

        private bool DeleteUser(Request request)
        {
            return dataContext.UserContext.Delete(request.UserKey.UserId);
        }

        private async Task EnsureIsUserExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.UserKey));
        }
    }
}