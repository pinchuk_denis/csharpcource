﻿using Infrastructure.Response;

namespace Domain.User.FromLocalStorage.Delete
{
    public class Response : TokenedDataResponse<SucceededResult>
    {
        public Response(bool isSucceeded)
            : base(new SucceededResult(isSucceeded))
        {
        }
    }
}