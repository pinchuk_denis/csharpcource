﻿using FluentValidation;
using Infrastructure.Validation;
using System;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Create
{
    public class RequestValidator : FluentValidator<Request>
    {
        public override Type LocalizationResourcesType => typeof(Resources);

        protected override Task SetValidationRulesAsync(ValidationContext<Request> context)
        {
            RuleFor(x => x.Name)
                .RequiredString()
                .WithErrorCode("Error_User_Name_Invalid");

            return Task.CompletedTask;
        }
    }
}