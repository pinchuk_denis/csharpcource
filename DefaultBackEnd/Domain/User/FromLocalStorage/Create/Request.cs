﻿using Infrastructure.Cqrs.Commands;

namespace Domain.User.FromLocalStorage.Create
{
    public class Request : ICommand<Response>
    {
        public string Name { get; set; }
    }
}