﻿using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Create.OnCreated
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IMediator mediator;

        public RequestHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected override async Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await mediator.Publish(new Notification
            {
                CretedUser = request.User
            });
        }
    }
}