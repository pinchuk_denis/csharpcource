﻿using MediatR;

namespace Domain.User.FromLocalStorage.Create.OnCreated
{
    public class Notification : INotification
    {
        public Data.User CretedUser { get; set; }
    }
}