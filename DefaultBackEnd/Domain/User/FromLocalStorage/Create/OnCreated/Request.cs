﻿using Infrastructure.Cqrs.Commands;

namespace Domain.User.FromLocalStorage.Create.OnCreated
{
    public class Request : ICommand
    {
        public Request(Data.User user)
        {
            User = user;
        }

        public Data.User User { get; }
    }
}