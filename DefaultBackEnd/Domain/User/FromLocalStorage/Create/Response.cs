﻿using Infrastructure.Response;

namespace Domain.User.FromLocalStorage.Create
{
    public class Response : TokenedDataResponse<UserKey>
    {
        public Response(UserKey result)
            : base(result)
        {
        }
    }
}