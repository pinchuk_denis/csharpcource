﻿using Data;
using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Create
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            var user = dataContext
                .UserContext
                .Create(
                    new Data.User
                    {
                        Name = request.Name
                    });

            await mediator.Send(new OnCreated.Request(user));

            return new Response(new UserKey(user.Id));
        }
    }
}