﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Edit
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsUserExists(request);

            bool isSucceeded = EditUser(request);

            return new Response(isSucceeded);
        }

        private bool EditUser(Request request)
        {
            var result = dataContext.UserContext.Edit(
                request.UserKey.UserId,
                new Data.User
                {
                    Name = request.Name
                });

            return result;
        }

        private async Task EnsureIsUserExists(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.UserKey));
        }
    }
}