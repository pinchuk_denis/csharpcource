﻿using Infrastructure.Sorting;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.User.FromLocalStorage.List
{
    public class SortingRules : SortingRules<Data.User, SortingField>
    {
        public override SortingRule<SortingField>[] DefaultSortingRules { get; } =
        {
            new SortingRule<SortingField>
            {
                PropertyName = SortingField.Id,
                SortDirection = SortDirection.Desc
            }
        };

        public override IReadOnlyDictionary<SortingField, Expression<Func<Data.User, object>>> SortingExpressions
        {
            get;
        } =
            new Dictionary<SortingField, Expression<Func<Data.User, object>>>
            {
                {
                    SortingField.Name,
                    x => x.Name
                },
                {
                    SortingField.Id,
                    x => x.Id
                }
            };
    }
}