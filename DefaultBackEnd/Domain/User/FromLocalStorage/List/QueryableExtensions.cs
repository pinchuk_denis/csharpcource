﻿using System.Linq;

namespace Domain.User.FromLocalStorage.List
{
    public static class QueryableExtensions
    {
        public static IQueryable<Data.User> ApplySearching(
            this IQueryable<Data.User> query,
            string searchText)
        {
            if (!string.IsNullOrWhiteSpace(searchText)) query = query.Where(x => x.Name == searchText);

            return query;
        }
    }
}