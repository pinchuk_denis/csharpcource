﻿namespace Domain.User.FromLocalStorage.List
{
    public enum SortingField
    {
        Name = 1,
        Id = 2
    }
}