﻿using Infrastructure.Paging;

namespace Domain.User.FromLocalStorage.List
{
    public class Response : TokenedListResponse<Data.User, SortingField>
    {
    }
}