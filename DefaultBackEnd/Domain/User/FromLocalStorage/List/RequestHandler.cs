﻿using Data;
using Infrastructure.Paging;
using Infrastructure.Sorting;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.List
{
    public class RequestHandler
        : PagedListRequestHandler<
            Request,
            Response,
            Data.User,
            SortingField>
    {
        private readonly IDataContext dataContext;

        private readonly SortingRules sortingRules;

        public RequestHandler(
            IDataContext dataContext,
            SortingRules sortingRules)
        {
            this.dataContext = dataContext;
            this.sortingRules = sortingRules;
        }

        public override Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var users = dataContext.UserContext.Get()
                .AsQueryable()
                .ApplySearching(request.SearchText)
                .ApplyOrdering(request, sortingRules)
                .ApplyPaging(request)
                .ToList();

            int totalCount = dataContext.UserContext.Get()
                .AsQueryable()
                .ApplySearching(request.SearchText)
                .Count();

            var result = MapToListResponse(users, request, totalCount);

            return Task.FromResult(result);
        }
    }
}