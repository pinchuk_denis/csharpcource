﻿using Domain.User.FromLocalStorage.Create.OnCreated;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.FromLocalStorage.Admin
{
    public class NotificationHandler : INotificationHandler<Notification>
    {
        public Task Handle(Notification notification, CancellationToken cancellationToken)
        {
            if (notification.CretedUser.Name.ToLower() == "admin") Count.Add();

            return Task.CompletedTask;
        }
    }
}