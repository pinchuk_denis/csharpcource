﻿namespace Domain.User.FromLocalStorage.FromLocalStorage.Admin
{
    public static class Count
    {
        private static int count;

        public static void Add()
        {
            count++;
        }

        public static int Get()
        {
            return count;
        }
    }
}