﻿using Data;
using Infrastructure.Cqrs.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromLocalStorage.Ensure.IsExist
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IDataContext dataContext;

        public RequestHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        protected override Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            bool isExist = dataContext.UserContext.Get().Any(x => x.Id == request.UserKey.UserId);

            if (!isExist) throw new UserNotFoundException(request.UserKey);

            return Task.CompletedTask;
        }
    }
}