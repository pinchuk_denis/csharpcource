﻿using Infrastructure.Response;

namespace Domain.User.FromDb.Details
{
    public class Response : TokenedDataResponse<DataDb.Entities.User>
    {
        public Response(DataDb.Entities.User result)
            : base(result)
        {
        }
    }
}