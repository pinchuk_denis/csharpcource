﻿using Data;
using Infrastructure.Cqrs.Queries;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Details
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly IMediator mediator;

        public RequestHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var user = await this.FindUserAsync(request.UserKey);

            return new Response(user);
        }

        private async Task<DataDb.Entities.User> FindUserAsync(UserKey userKey)
        {
            var result = (await mediator.Send(new Find.Request(userKey))).Data;

            return result;
        }
    }
}