﻿using Infrastructure.Cqrs.Commands;

namespace Domain.User.FromDb.Ensure.IsExist
{
    public class Request : ICommand
    {
        public Request(UserKey userKey)
        {
            UserKey = userKey;
        }

        public UserKey UserKey { get; }
    }
}