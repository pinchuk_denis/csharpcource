﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Ensure.IsExist
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected override async Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            var user = await this.FindUserAsync(request.UserKey);

            if (user == null) throw new UserNotFoundException(request.UserKey);
        }

        private async Task<DataDb.Entities.User> FindUserAsync(UserKey userKey)
        {
            var result = (await this.mediator.Send(new Find.Request(userKey))).Data;

            return result;
        }
    }
}