﻿using DataDb.Entities;
using Infrastructure.Cqrs.Queries;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Find
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;

        public RequestHandler(DataContextDb dataContextDb)
        {
            this.dataContextDb = dataContextDb;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var user = await FindUserAsync(request);

            return new Response(user);
        }

        private async Task<DataDb.Entities.User> FindUserAsync(Request request)
        {
            return await this.dataContextDb.Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.UserId == request.UserKey.UserId);
        }
    }
}