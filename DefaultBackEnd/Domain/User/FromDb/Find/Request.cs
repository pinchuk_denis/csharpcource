﻿using Infrastructure.Cqrs.Queries;

namespace Domain.User.FromDb.Find
{
    public class Request : IQuery<Response>
    {
        public Request(UserKey userKey)
        {
            UserKey = userKey;
        }

        public UserKey UserKey { get; }
    }
}