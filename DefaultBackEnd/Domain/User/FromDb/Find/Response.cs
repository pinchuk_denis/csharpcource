﻿using Infrastructure.Response;

namespace Domain.User.FromDb.Find
{
    public class Response : TokenedDataResponse<DataDb.Entities.User>
    {
        public Response(DataDb.Entities.User result)
            : base(result)
        {
        }
    }
}