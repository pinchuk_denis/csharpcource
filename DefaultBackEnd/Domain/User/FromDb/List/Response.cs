﻿using Infrastructure.Paging;

namespace Domain.User.FromDb.List
{
    public class Response : TokenedListResponse<ListItem, SortingField>
    {
    }
}