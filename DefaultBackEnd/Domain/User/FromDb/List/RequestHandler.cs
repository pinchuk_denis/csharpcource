﻿using DataDb.Entities;
using Infrastructure.Paging;
using Infrastructure.Sorting;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.List
{
    public class RequestHandler
        : PagedListRequestHandler<
            Request,
            Response,
            ListItem,
            SortingField>
    {
        private readonly DataContextDb dataContextDb;
        private readonly SortingRules sortingRules;

        public RequestHandler(
            DataContextDb dataContextDb,
            SortingRules sortingRules)
        {
            this.dataContextDb = dataContextDb;
            this.sortingRules = sortingRules;
        }

        public override async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var users = await this.dataContextDb.Users
                .AsNoTracking()
                .ApplySearching(request.SearchText)
                .ApplyOrdering(request, sortingRules)
                .ApplyPaging(request)
                .ToListItem()
                .ToListAsync();

            int totalCount = await this.dataContextDb.Users
                .AsNoTracking()
                .ApplySearching(request.SearchText)
                .CountAsync();

            var result = MapToListResponse(users, request, totalCount);

            return result;
        }
    }
}