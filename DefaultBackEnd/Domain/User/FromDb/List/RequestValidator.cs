﻿using FluentValidation;
using Infrastructure.Paging;
using System;
using System.Threading.Tasks;

namespace Domain.User.FromDb.List
{
    public class RequestValidator : PagedListRequestValidator<
        Request,
        Response,
        SortingField>
    {
        public override Type LocalizationResourcesType => typeof(Resources);

        protected override Task ValidateFieldsAsync(ValidationContext<Request> context)
        {
            return Task.CompletedTask;
        }
    }
}