﻿using Domain.Role;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.User.FromDb.List
{
    public static class QueryableExtensions
    {
        public static IQueryable<DataDb.Entities.User> ApplySearching(
            this IQueryable<DataDb.Entities.User> query,
            string searchText)
        {
            if (!string.IsNullOrWhiteSpace(searchText)) query = query.Where(x => EF.Functions.Like(x.Name, $"{searchText}%"));

            return query;
        }

        public static IQueryable<ListItem> ToListItem(
            this IQueryable<DataDb.Entities.User> query)
        {
            var result = from item in query
                         select new ListItem
                         {
                             UserItem = new UserItem
                             {
                                 Id = new UserKey(item.UserId),
                                 Name = item.Name
                             },

                             RoleItem = new RoleItem
                             {
                                 Id = new RoleKey(item.Role.RoleId),
                                 Name = item.Role.Name
                             }
                         };

            return result;
        }
    }
}