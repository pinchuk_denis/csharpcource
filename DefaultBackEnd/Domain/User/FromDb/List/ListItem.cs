﻿using Domain.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.User.FromDb.List
{
    public class ListItem
    {
        public UserItem UserItem { get; set; }

        public RoleItem RoleItem { get; set; }
    }

    public class UserItem
    {
        public UserKey Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleItem
    {
        public RoleKey Id { get; set; }
        public string Name { get; set; }
    }
}
