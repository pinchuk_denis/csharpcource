﻿using Infrastructure.Paging;

namespace Domain.User.FromDb.List
{
    public class Request : PagedListRequest<Response, SortingField>
    {
    }
}