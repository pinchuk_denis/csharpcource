﻿using Data;
using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Edit
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;
        private readonly IMediator mediator;

        public RequestHandler(DataContextDb dataContextDb, IMediator mediator)
        {
            this.dataContextDb = dataContextDb;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsUserExists(request);

            var user = await this.FindUserAsync(request.UserKey);

            bool isSucceeded = await this.EditUserAsync(user, request);

            return new Response(isSucceeded);
        }

        private async Task<bool> EditUserAsync(DataDb.Entities.User user, Request request)
        {
            user.Name = request.Name;
            user.RoleId = request.Role.RoleId;

            await this.dataContextDb.SaveChangesAsync();

            return true;
        }

        private async Task EnsureIsUserExists(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.UserKey));
        }

        private async Task<DataDb.Entities.User> FindUserAsync(UserKey userKey)
        {
            var result = (await mediator.Send(new Find.Request(userKey))).Data;

            return result;
        }
    }
}