﻿using Domain.Role;
using Infrastructure.Cqrs.Commands;
using System.Text.Json.Serialization;

namespace Domain.User.FromDb.Edit
{
    public class Request : ICommand<Response>
    {
        [JsonIgnore] 
        public UserKey UserKey { get; set; }

        public string Name { get; set; }

        public RoleKey Role{ get; set; }
    }
}