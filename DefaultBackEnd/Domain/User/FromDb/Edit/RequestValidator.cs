﻿using Domain.Role;
using FluentValidation;
using Infrastructure.Validation;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Edit
{
    public class RequestValidator : FluentValidator<Request>
    {
        private readonly IMediator mediator;

        public RequestValidator(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public override Type LocalizationResourcesType => null;

        protected override Task SetValidationRulesAsync(ValidationContext<Request> context)
        {
            RuleFor(x => x.Name)
                .RequiredString()
                .WithErrorCode("Error_User_Name_Invalid");

            RuleFor(x => x.Role)
                .NotNull()
                .WithErrorCode("Error_User_Role_Null")
                .MustAsync(IsRoleExistAsync)
                .WithErrorCode("Error_User_Role_NotExist");

            return Task.CompletedTask;
        }

        private async Task<bool> IsRoleExistAsync(RoleKey roleKey, CancellationToken cancellationToken)
        {
            bool isRoleIxist = (await this.mediator.Send(new Domain.Role.FromDb.IsExist.Request(roleKey))).Data;

            return isRoleIxist;
        }
    }
}