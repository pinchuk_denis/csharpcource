﻿using Infrastructure.Response;

namespace Domain.User.FromDb.Edit
{
    public class Response : TokenedDataResponse<SucceededResult>
    {
        public Response(bool isSucceeded)
            : base(new SucceededResult(isSucceeded))
        {
        }
    }
}