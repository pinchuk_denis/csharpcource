﻿using Infrastructure.Cqrs.Commands;

namespace Domain.User.FromDb.Delete
{
    public class Request : ICommand<Response>
    {
        public Request(UserKey userKey)
        {
            UserKey = userKey;
        }

        public UserKey UserKey { get; }
    }
}