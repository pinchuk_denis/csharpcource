﻿using Infrastructure.Response;

namespace Domain.User.FromDb.Delete
{
    public class Response : TokenedDataResponse<bool>
    {
        public Response(bool result)
            : base(result)
        {
        }
    }
}