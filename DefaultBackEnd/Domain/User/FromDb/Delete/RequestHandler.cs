﻿using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Delete
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;
       
        private readonly IMediator mediator;

        public RequestHandler(DataContextDb dataContextDb, IMediator mediator)
        {
            this.dataContextDb = dataContextDb;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsUserExistsAsync(request);

            var user = await this.FindUserAsync(request.UserKey);

            bool isSucceeded = await this.DeleteUserAsync(user);

            return new Response(isSucceeded);
        }

        private async Task<DataDb.Entities.User> FindUserAsync(UserKey userKey)
        {
            var result = (await mediator.Send(new Find.Request(userKey))).Data;

            return result;
        }

        private async Task<bool> DeleteUserAsync(DataDb.Entities.User user)
        {
            var result = true;
            
            this.dataContextDb.Users.Remove(user);

            await this.dataContextDb.SaveChangesAsync();

            return result;
        }

        private async Task EnsureIsUserExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.UserKey));
        }
    }
}