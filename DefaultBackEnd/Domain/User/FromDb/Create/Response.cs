﻿using Infrastructure.Response;

namespace Domain.User.FromDb.Create
{
    public class Response : TokenedDataResponse<UserKey>
    {
        public Response(UserKey result)
            : base(result)
        {
        }
    }
}