﻿using Domain.Role;
using Infrastructure.Cqrs.Commands;

namespace Domain.User.FromDb.Create
{
    public class Request : ICommand<Response>
    {
        public string Name { get; set; }
        
        public RoleKey Role{ get; set; }
    }
}