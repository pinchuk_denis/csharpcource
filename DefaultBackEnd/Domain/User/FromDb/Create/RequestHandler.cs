﻿using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.User.FromDb.Create
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;

        public RequestHandler(DataContextDb dataContextDb)
        {
            this.dataContextDb = dataContextDb;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            DataDb.Entities.User user = NewUser(request);

            await SaveToDbAsync(user);

            return new Response(new UserKey(user.UserId));
        }

        private async Task SaveToDbAsync(DataDb.Entities.User user)
        {
            dataContextDb.Users.Add(user);

            await this.dataContextDb.SaveChangesAsync();
        }

        private static DataDb.Entities.User NewUser(Request request)
        {
            return new DataDb.Entities.User
            {
                Name = request.Name,
                RoleId = request.Role.RoleId
            };
        }
    }
}