﻿using Infrastructure.Exceptions;
using System;

namespace Domain.User
{
    public class UserNotFoundException : BusinessException
    {
        public UserNotFoundException(UserKey userKey)
            : base(new ErrorDescription
            {
                Key = ErrorKeys.RequestError,
                Code = "Error_User_NotFound",
                Message = $"User not found. Id - {userKey}",
                SystemMessage = $"User not found. Id - {userKey}"
            })
        {
        }

        public override Type LocalizationResourcesType => typeof(Resources);
    }
}