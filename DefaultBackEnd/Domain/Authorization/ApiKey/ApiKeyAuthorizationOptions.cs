﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Authorization.ApiKey
{
	public class ApiKeyAuthorizationOptions
	{
		public string ApiKey { get; set; }
	}
}
