﻿using Data;
using Infrastructure.Cqrs.Commands;
using Infrastructure.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Authorization.ApiKey.Validate
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IOptions<ApiKeyAuthorizationOptions> options;

        private readonly HttpContext context;


        public RequestHandler(
            IOptions<ApiKeyAuthorizationOptions> options,
            IHttpContextAccessor httpContextAccessor)
        {
            this.options = options;
            this.context = httpContextAccessor.HttpContext;
        }

        protected override  Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            string apiKey = this.ApiKey() ;

            if (string.IsNullOrEmpty(apiKey) || apiKey != this.options.Value.ApiKey)
            {
                throw new UnauthorizedException();
            }

            return Task.CompletedTask;
        }

        private string ApiKey()
        {
            var xApiKeyHeaderValues = this.context.Request.Headers["X-Api-Key"];

            string result = xApiKeyHeaderValues.Any() ? xApiKeyHeaderValues[0] : null;

            return result;
        }
    }
}