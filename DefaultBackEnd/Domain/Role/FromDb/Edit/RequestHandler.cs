﻿using Data;
using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Edit
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;
        private readonly IMediator mediator;

        public RequestHandler(DataContextDb dataContextDb, IMediator mediator)
        {
            this.dataContextDb = dataContextDb;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsRoleExists(request);

            var role = await this.FindRoleAsync(request.RoleKey);

            bool isSucceeded = await this.EditRoleAsync(role, request);

            return new Response(isSucceeded);
        }

        private async Task<bool> EditRoleAsync(DataDb.Entities.Role role, Request request)
        {
            role.Name = request.Name;

            await this.dataContextDb.SaveChangesAsync();

            return true;
        }

        private async Task EnsureIsRoleExists(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.RoleKey));
        }

        private async Task<DataDb.Entities.Role> FindRoleAsync(RoleKey roleKey)
        {
            var result = (await mediator.Send(new Find.Request(roleKey))).Data;

            return result;
        }
    }
}