﻿using Infrastructure.Response;

namespace Domain.Role.FromDb.Edit
{
    public class Response : TokenedDataResponse<SucceededResult>
    {
        public Response(bool isSucceeded)
            : base(new SucceededResult(isSucceeded))
        {
        }
    }
}