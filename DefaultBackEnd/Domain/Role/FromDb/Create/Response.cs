﻿using Infrastructure.Response;

namespace Domain.Role.FromDb.Create
{
    public class Response : TokenedDataResponse<RoleKey>
    {
        public Response(RoleKey result)
            : base(result)
        {
        }
    }
}