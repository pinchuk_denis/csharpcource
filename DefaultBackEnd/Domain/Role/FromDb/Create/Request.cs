﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromDb.Create
{
    public class Request : ICommand<Response>
    {
        public string Name { get; set; }
    }
}