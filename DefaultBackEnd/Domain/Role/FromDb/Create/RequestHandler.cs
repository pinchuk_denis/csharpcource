﻿using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Create
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;

        public RequestHandler(DataContextDb dataContextDb)
        {
            this.dataContextDb = dataContextDb;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            DataDb.Entities.Role role = NewRole(request);

            await SaveToDbAsync(role);

            return new Response(new RoleKey(role.RoleId));
        }

        private async Task SaveToDbAsync(DataDb.Entities.Role role)
        {
            dataContextDb.Roles.Add(role);

            await this.dataContextDb.SaveChangesAsync();
        }

        private static DataDb.Entities.Role NewRole(Request request)
        {
            return new DataDb.Entities.Role
            {
                Name = request.Name
            };
        }
    }
}