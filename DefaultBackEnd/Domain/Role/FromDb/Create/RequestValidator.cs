﻿using FluentValidation;
using Infrastructure.Validation;
using System;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Create
{
    public class RequestValidator : FluentValidator<Request>
    {
        public override Type LocalizationResourcesType => null;

        protected override Task SetValidationRulesAsync(ValidationContext<Request> context)
        {
            RuleFor(x => x.Name)
                .RequiredString()
                .WithErrorCode("Error_Role_Name_Invalid");

            return Task.CompletedTask;
        }
    }
}