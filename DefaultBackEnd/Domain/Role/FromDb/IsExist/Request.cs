﻿using Infrastructure.Cqrs.Queries;

namespace Domain.Role.FromDb.IsExist
{
    public class Request : IQuery<Response>
    {
        public Request(RoleKey roleKey)
        {
            RoleKey = roleKey;
        }

        public RoleKey RoleKey { get; }
    }
}