﻿using Infrastructure.Response;

namespace Domain.Role.FromDb.IsExist
{
    public class Response : DataResponse<bool>
    {
        public Response(bool result)
            : base(result)
        {
        }
    }
}