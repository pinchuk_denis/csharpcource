﻿using DataDb.Entities;
using Infrastructure.Cqrs.Queries;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.IsExist
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;

        public RequestHandler(DataContextDb dataContextDb)
        {
            this.dataContextDb = dataContextDb;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var isRoleExist = await IsRoleExistAsync(request);

            return new Response(isRoleExist);
        }

        private async Task<bool> IsRoleExistAsync(Request request)
        {
            return await this.dataContextDb.Roles.AnyAsync(x => x.RoleId == request.RoleKey.RoleId);
        }
    }
}