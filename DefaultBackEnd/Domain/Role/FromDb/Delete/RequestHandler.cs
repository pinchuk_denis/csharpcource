﻿using DataDb.Entities;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Delete
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;
       
        private readonly IMediator mediator;

        public RequestHandler(DataContextDb dataContextDb, IMediator mediator)
        {
            this.dataContextDb = dataContextDb;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsRoleExistsAsync(request);

            //TODO: проверить по аналогии EnsureIsRoleExistsAsync есть ли заасаненные юзеры на роль

            var role = await this.FindRoleAsync(request.RoleKey);

            bool isSucceeded = await this.DeleteRoleAsync(role);

            return new Response(isSucceeded);
        }

        private async Task<DataDb.Entities.Role> FindRoleAsync(RoleKey roleKey)
        {
            var result = (await mediator.Send(new Find.Request(roleKey))).Data;

            return result;
        }

        private async Task<bool> DeleteRoleAsync(DataDb.Entities.Role role)
        {
            var result = true;
            
            this.dataContextDb.Roles.Remove(role);

            await this.dataContextDb.SaveChangesAsync();

            return result;
        }

        private async Task EnsureIsRoleExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.RoleKey));
        }
    }
}