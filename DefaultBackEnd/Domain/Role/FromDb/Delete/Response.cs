﻿using Infrastructure.Response;

namespace Domain.Role.FromDb.Delete
{
    public class Response : TokenedDataResponse<bool>
    {
        public Response(bool result)
            : base(result)
        {
        }
    }
}