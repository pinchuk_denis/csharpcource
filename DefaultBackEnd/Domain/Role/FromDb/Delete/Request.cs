﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromDb.Delete
{
    public class Request : ICommand<Response>
    {
        public Request(RoleKey roleKey)
        {
            RoleKey = roleKey;
        }

        public RoleKey RoleKey { get; }
    }
}