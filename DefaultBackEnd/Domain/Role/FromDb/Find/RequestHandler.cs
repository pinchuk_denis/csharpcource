﻿using DataDb.Entities;
using Infrastructure.Cqrs.Queries;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Find
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly DataContextDb dataContextDb;

        public RequestHandler(DataContextDb dataContextDb)
        {
            this.dataContextDb = dataContextDb;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var role = await FindRoleAsync(request);

            return new Response(role);
        }

        private async Task<DataDb.Entities.Role> FindRoleAsync(Request request)
        {
            return await this.dataContextDb.Roles.FirstOrDefaultAsync(x => x.RoleId == request.RoleKey.RoleId);
        }
    }
}