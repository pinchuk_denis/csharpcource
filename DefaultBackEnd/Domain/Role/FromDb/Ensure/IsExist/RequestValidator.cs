﻿using FluentValidation;
using Infrastructure.Validation;
using System;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Ensure.IsExist
{
    public class RequestValidator : FluentValidator<Request>
    {
        public override Type LocalizationResourcesType => null;

        protected override Task SetValidationRulesAsync(ValidationContext<Request> context)
        {
            return Task.CompletedTask;
        }
    }
}