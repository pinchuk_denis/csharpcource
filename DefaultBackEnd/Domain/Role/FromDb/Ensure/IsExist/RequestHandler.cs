﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Ensure.IsExist
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IMediator mediator;

        public RequestHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected override async Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            var role = await this.FindRoleAsync(request.RoleKey);

            if (role == null) throw new RoleNotFoundException(request.RoleKey);
        }

        private async Task<DataDb.Entities.Role> FindRoleAsync(RoleKey roleKey)
        {
            var result = (await this.mediator.Send(new Find.Request(roleKey))).Data;

            return result;
        }
    }
}