﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromDb.Ensure.IsExist
{
    public class Request : ICommand
    {
        public Request(RoleKey roleKey)
        {
            RoleKey = roleKey;
        }

        public RoleKey RoleKey { get; }
    }
}