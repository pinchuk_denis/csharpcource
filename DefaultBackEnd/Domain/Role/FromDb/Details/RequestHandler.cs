﻿using Data;
using Infrastructure.Cqrs.Queries;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.Details
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly IMediator mediator;

        public RequestHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var role = await this.FindRoleAsync(request.RoleKey);

            return new Response(role);
        }

        private async Task<DataDb.Entities.Role> FindRoleAsync(RoleKey roleKey)
        {
            var result = (await mediator.Send(new Find.Request(roleKey))).Data;

            return result;
        }
    }
}