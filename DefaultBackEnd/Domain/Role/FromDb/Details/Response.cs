﻿using Infrastructure.Response;

namespace Domain.Role.FromDb.Details
{
    public class Response : TokenedDataResponse<DataDb.Entities.Role>
    {
        public Response(DataDb.Entities.Role result)
            : base(result)
        {
        }
    }
}