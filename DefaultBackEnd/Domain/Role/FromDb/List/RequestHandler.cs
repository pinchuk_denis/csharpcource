﻿using DataDb.Entities;
using Infrastructure.Paging;
using Infrastructure.Sorting;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromDb.List
{
    public class RequestHandler
        : PagedListRequestHandler<
            Request,
            Response,
            DataDb.Entities.Role,
            SortingField>
    {
        private readonly DataContextDb dataContextDb;
        private readonly SortingRules sortingRules;

        public RequestHandler(
            DataContextDb dataContextDb,
            SortingRules sortingRules)
        {
            this.dataContextDb = dataContextDb;
            this.sortingRules = sortingRules;
        }

        public override async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var roles = await this.dataContextDb.Roles
                .AsNoTracking()
                .Include(x => x.Users)
                .ApplySearching(request.SearchText)
                .ApplyOrdering(request, sortingRules)
                .ApplyPaging(request)
                .ToListAsync();

            int totalCount = await this.dataContextDb.Roles
                .AsNoTracking()
                .ApplySearching(request.SearchText)
                .CountAsync();

            var result = MapToListResponse(roles, request, totalCount);

            return result;
        }
    }
}