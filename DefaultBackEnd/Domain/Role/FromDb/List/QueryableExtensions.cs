﻿using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Domain.Role.FromDb.List
{
    public static class QueryableExtensions
    {
        public static IQueryable<DataDb.Entities.Role> ApplySearching(
            this IQueryable<DataDb.Entities.Role> query,
            string searchText)
        {
            if (!string.IsNullOrWhiteSpace(searchText)) query = query.Where(x => EF.Functions.Like(x.Name, $"{searchText}%"));

            return query;
        }
    }
}