﻿using Infrastructure.Sorting;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Role.FromDb.List
{
    public class SortingRules : SortingRules<DataDb.Entities.Role, SortingField>
    {
        public override SortingRule<SortingField>[] DefaultSortingRules { get; } =
        {
            new SortingRule<SortingField>
            {
                PropertyName = SortingField.Id,
                SortDirection = SortDirection.Desc
            }
        };

        public override IReadOnlyDictionary<SortingField, Expression<Func<DataDb.Entities.Role, object>>> SortingExpressions
        {
            get;
        } =
            new Dictionary<SortingField, Expression<Func<DataDb.Entities.Role, object>>>
            {
                {
                    SortingField.Name,
                    x => x.Name
                },
                {
                    SortingField.Id,
                    x => x.RoleId
                }
            };
    }
}