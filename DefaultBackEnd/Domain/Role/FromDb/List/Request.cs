﻿using Infrastructure.Paging;

namespace Domain.Role.FromDb.List
{
    public class Request : PagedListRequest<Response, SortingField>
    {
    }
}