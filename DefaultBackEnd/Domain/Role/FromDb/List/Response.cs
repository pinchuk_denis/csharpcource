﻿using Infrastructure.Paging;

namespace Domain.Role.FromDb.List
{
    public class Response : TokenedListResponse<DataDb.Entities.Role, SortingField>
    {
    }
}