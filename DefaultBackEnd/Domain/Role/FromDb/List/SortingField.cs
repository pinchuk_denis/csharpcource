﻿namespace Domain.Role.FromDb.List
{
    public enum SortingField
    {
        Name = 1,
        Id = 2
    }
}