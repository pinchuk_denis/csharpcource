﻿using System.Linq;

namespace Domain.Role.FromLocalStorage.List
{
    public static class QueryableExtensions
    {
        public static IQueryable<Data.Role> ApplySearching(
            this IQueryable<Data.Role> query,
            string searchText)
        {
            if (!string.IsNullOrWhiteSpace(searchText)) query = query.Where(x => x.Name == searchText);

            return query;
        }
    }
}