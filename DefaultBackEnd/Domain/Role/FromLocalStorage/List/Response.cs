﻿using Infrastructure.Paging;

namespace Domain.Role.FromLocalStorage.List
{
    public class Response : TokenedListResponse<Data.Role, SortingField>
    {
    }
}