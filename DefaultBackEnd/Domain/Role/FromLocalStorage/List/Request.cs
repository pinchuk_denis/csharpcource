﻿using Infrastructure.Paging;

namespace Domain.Role.FromLocalStorage.List
{
    public class Request : PagedListRequest<Response, SortingField>
    {
    }
}