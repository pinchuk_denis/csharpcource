﻿using Data;
using Infrastructure.Cqrs.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromLocalStorage.Create
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;

        public RequestHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        protected override Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            var user = dataContext
                .RoleContext
                .Create(
                    new Data.Role
                    {
                        Name = request.Name
                    });

            return Task.FromResult(new Response(new RoleKey(user.Id)));
        }
    }
}