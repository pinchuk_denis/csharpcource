﻿using Infrastructure.Response;

namespace Domain.Role.FromLocalStorage.Create
{
    public class Response : TokenedDataResponse<RoleKey>
    {
        public Response(RoleKey result)
            : base(result)
        {
        }
    }
}