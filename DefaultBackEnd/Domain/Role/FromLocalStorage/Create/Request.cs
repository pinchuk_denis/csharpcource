﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromLocalStorage.Create
{
    public class Request : ICommand<Response>
    {
        public string Name { get; set; }
    }
}