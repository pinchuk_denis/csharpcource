﻿using Data;
using Infrastructure.Cqrs.Commands;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromLocalStorage.Ensure.IsExist
{
    public class RequestHandler : CommandHandler<Request>
    {
        private readonly IDataContext dataContext;

        public RequestHandler(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        protected override Task HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            bool isExist = dataContext.RoleContext.Get().Any(x => x.Id == request.RoleKey.RoleId);

            if (!isExist) throw new RoleNotFoundException(request.RoleKey);

            return Task.CompletedTask;
        }
    }
}