﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromLocalStorage.Ensure.IsExist
{
    public class Request : ICommand
    {
        public Request(RoleKey roleKey)
        {
            RoleKey = roleKey;
        }

        public RoleKey RoleKey { get; }
    }
}