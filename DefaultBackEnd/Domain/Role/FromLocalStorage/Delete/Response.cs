﻿using Infrastructure.Response;

namespace Domain.Role.FromLocalStorage.Delete
{
    public class Response : TokenedDataResponse<bool>
    {
        public Response(bool result)
            : base(result)
        {
        }
    }
}