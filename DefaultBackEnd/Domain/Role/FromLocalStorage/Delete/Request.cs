﻿using Infrastructure.Cqrs.Commands;

namespace Domain.Role.FromLocalStorage.Delete
{
    public class Request : ICommand<Response>
    {
        public Request(RoleKey roleKey)
        {
            RoleKey = roleKey;
        }

        public RoleKey RoleKey { get; }
    }
}