﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromLocalStorage.Delete
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsRoleExistsAsync(request);

            bool isSucceeded = DeleteRole(request);

            return new Response(isSucceeded);
        }

        private bool DeleteRole(Request request)
        {
            return dataContext.RoleContext.Delete(request.RoleKey.RoleId);
        }

        private async Task EnsureIsRoleExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.RoleKey));
        }
    }
}