﻿using Data;
using Infrastructure.Cqrs.Queries;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromLocalStorage.Details
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsRoleExistsAsync(request);

            var role = GetRoleDetails(request);

            return new Response(role);
        }

        private Data.Role GetRoleDetails(Request request)
        {
            return dataContext.RoleContext.Get().FirstOrDefault(x => x.Id == request.RoleKey.RoleId);
        }

        private async Task EnsureIsRoleExistsAsync(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.RoleKey));
        }
    }
}