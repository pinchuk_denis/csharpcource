﻿using Infrastructure.Response;

namespace Domain.Role.FromLocalStorage.Details
{
    public class Response : TokenedDataResponse<Data.Role>
    {
        public Response(Data.Role result)
            : base(result)
        {
        }
    }
}