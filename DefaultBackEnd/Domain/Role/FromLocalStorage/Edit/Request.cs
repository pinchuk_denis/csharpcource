﻿using Infrastructure.Cqrs.Commands;
using System.Text.Json.Serialization;

namespace Domain.Role.FromLocalStorage.Edit
{
    public class Request : ICommand<Response>
    {
        [JsonIgnore] 
        public RoleKey RoleKey { get; set; }

        public string Name { get; set; }
    }
}