﻿using Infrastructure.Response;

namespace Domain.Role.FromLocalStorage.Edit
{
    public class Response : TokenedDataResponse<SucceededResult>
    {
        public Response(bool isSucceeded)
            : base(new SucceededResult(isSucceeded))
        {
        }
    }
}