﻿using Data;
using Infrastructure.Cqrs.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Role.FromLocalStorage.Edit
{
    public class RequestHandler : CommandHandler<Request, Response>
    {
        private readonly IDataContext dataContext;
        private readonly IMediator mediator;

        public RequestHandler(IDataContext dataContext, IMediator mediator)
        {
            this.dataContext = dataContext;
            this.mediator = mediator;
        }

        protected override async Task<Response> HandleRequestAsync(Request request, CancellationToken cancellationToken)
        {
            await EnsureIsRoleExists(request);

            bool isSucceeded = EditRole(request);

            return new Response(isSucceeded);
        }

        private bool EditRole(Request request)
        {
            var result = dataContext.RoleContext.Edit(
                request.RoleKey.RoleId,
                new Data.Role
                {
                    Name = request.Name
                });

            return result;
        }

        private async Task EnsureIsRoleExists(Request request)
        {
            await mediator.Send(new Ensure.IsExist.Request(request.RoleKey));
        }
    }
}