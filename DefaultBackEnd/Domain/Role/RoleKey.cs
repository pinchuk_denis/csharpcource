﻿namespace Domain.Role
{
    public class RoleKey
    {
        public RoleKey(int roleId)
        {
            RoleId = roleId;
        }

        public int RoleId { get; }
    }
}