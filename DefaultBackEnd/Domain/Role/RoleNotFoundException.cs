﻿using Infrastructure.Exceptions;
using System;

namespace Domain.Role
{
    public class RoleNotFoundException : BusinessException
    {
        public RoleNotFoundException(RoleKey roleKey)
            : base(new ErrorDescription
            {
                Key = ErrorKeys.RequestError,
                Code = "Error_Role_NotFound",
                Message = $"Role not found. Id - {roleKey}",
                SystemMessage = $"Role not found. Id - {roleKey}"
            })
        {
        }

        public override Type LocalizationResourcesType => typeof(Resources);
    }
}