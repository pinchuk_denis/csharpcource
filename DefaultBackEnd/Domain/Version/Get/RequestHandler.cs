﻿using Infrastructure;
using Infrastructure.Cqrs.Queries;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.Version.Get
{
    public class RequestHandler : IQueryHandler<Request, Response>
    {
        private readonly IConfiguration configuration;

        public RequestHandler(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public Task<Response> Handle(Request request, CancellationToken cancellationToken)
        {
            var versionResponse = new Response {Data = {Version = configuration.GetRequiredValue<string>("Application:AppVersion") } };

            return Task.FromResult(versionResponse);
        }
    }
}