﻿using Infrastructure.Response;

namespace Domain.Version.Get
{
    public class Response : DataResponse<Response.VersionResult>
    {
        public Response()
            : base(new VersionResult())
        {
        }

        public class VersionResult
        {
            public string Version { get; set; }
        }
    }
}