﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Cqrs.Queries;

namespace Infrastructure.Paging
{
    public abstract class
        PagedListRequestHandler<TRequest, TTokenedResponse, TListItem, TSortingField> : IQueryHandler<TRequest,
            TTokenedResponse>
        where TRequest : PagedListRequest<TTokenedResponse, TSortingField>
        where TTokenedResponse : TokenedListResponse<TListItem, TSortingField>, new()
    {
        public abstract Task<TTokenedResponse> Handle(TRequest message, CancellationToken cancellationToken);

        public TTokenedResponse MapToListResponse(IEnumerable<TListItem> list, TRequest request, int totalCount)
        {
            return new TTokenedResponse
            {
                Data = list,
                Sorting = request.Sorting,
                Pagination = new PaginationResponseModel
                {
                    PageNumber = request.PageNumber,
                    PageSize = request.PageSize,
                    TotalCount = totalCount
                }
            };
        }
    }
}