﻿namespace Infrastructure.Paging
{
    public interface IPagedResponse
    {
        PaginationResponseModel Pagination { get; set; }
    }
}