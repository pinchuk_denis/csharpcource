﻿namespace Infrastructure.Paging
{
    public class Paging : IPagedRequest
    {
        public Paging(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
        }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }
    }
}