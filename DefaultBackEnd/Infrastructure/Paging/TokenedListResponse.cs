﻿using System.Collections.Generic;
using Infrastructure.Response;
using Infrastructure.Sorting;

namespace Infrastructure.Paging
{
    public abstract class TokenedListResponse<TListItem, TSortingField> :
        TokenedDataResponse<IEnumerable<TListItem>>,
        ISortedResponse<TSortingField>,
        IPagedResponse
    {
        protected TokenedListResponse()
        {
        }

        protected TokenedListResponse(IEnumerable<TListItem> list) : base(list)
        {
        }

        public PaginationResponseModel Pagination { get; set; }

        public SortingRule<TSortingField>[] Sorting { get; set; }
    }
}