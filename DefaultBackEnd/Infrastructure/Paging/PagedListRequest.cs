﻿using Infrastructure.Cqrs.Queries;
using Infrastructure.Sorting;

namespace Infrastructure.Paging
{
    public abstract class PagedListRequest<TResponse, TSortingField> : IQuery<TResponse>, IPagedRequest,
        ISortedRequest<TSortingField>
    {
        public string SearchText { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public virtual SortingRule<TSortingField>[] Sorting { get; set; }
    }
}