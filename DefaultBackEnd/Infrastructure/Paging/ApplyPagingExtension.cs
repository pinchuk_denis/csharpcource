﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Infrastructure.Paging
{
    public static class ApplyPagingExtension
    {
        private const int PageNumberDefaultValue = 1;

        private const int PageSizeMaxValue = 5000;

        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> source, IPagedRequest pagedRequest)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            if (pagedRequest == null) throw new ArgumentNullException(nameof(pagedRequest));

            if (pagedRequest.PageSize > PageSizeMaxValue)
            {
                var error = $"{nameof(pagedRequest.PageSize)} can not be more than {PageSizeMaxValue}";
                throw new InvalidOperationException(error);
            }

            var pageNumber = GetPageNumber(pagedRequest.PageNumber);
            var pageSize = GetPageSize(pagedRequest.PageSize);

            return source.Page(pageNumber, pageSize);
        }

        private static int GetPageNumber(int sourcePageNumber)
        {
            var result = PageNumberDefaultValue;

            if (sourcePageNumber > 0) result = sourcePageNumber;

            return result;
        }

        private static int GetPageSize(int sourcePageSize)
        {
            var result = PageSizeMaxValue;

            if (sourcePageSize > 0 && sourcePageSize <= PageSizeMaxValue) result = sourcePageSize;

            return result;
        }
    }
}