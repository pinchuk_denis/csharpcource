﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Infrastructure.Validation;

namespace Infrastructure.Paging
{
    public abstract class
        PagedListRequestValidator<TPagedListRequest, TResponse, TSortingField> : FluentValidator<TPagedListRequest>
        where TPagedListRequest : PagedListRequest<TResponse, TSortingField>
    {
        public override Type LocalizationResourcesType => null;

        protected override async Task SetValidationRulesAsync(ValidationContext<TPagedListRequest> context)
        {
            RuleFor(x => x.SearchText)
                .MaximumLength(256)
                .WithErrorCode("Error_PagedListRequest_SearchText_TooLong");

            await ValidateFieldsAsync(context);
        }

        protected abstract Task ValidateFieldsAsync(ValidationContext<TPagedListRequest> context);
    }
}