using System;
using FluentValidation;
using Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Handlers
{
    public class ErrorHandler : ISingletonProvider
    {
        public ExceptionHandlerResult Handle(Exception exception, IServiceProvider serviceProvider)
        {
            ExceptionHandlerResult result;

            switch (exception)
            {
                case ModelBindingException modelBindingException:
                {
                    var exceptionHandler = serviceProvider.GetService<ModelBindingExceptionHandler>();
                    result = exceptionHandler.Handle(modelBindingException);

                    break;
                }
                case ValidationException validationException:
                {
                    var exceptionHandler = serviceProvider.GetService<ValidationExceptionHandler>();
                    result = exceptionHandler.Handle(validationException);
                    break;
                }
                case DbUpdateConcurrencyException dbUpdateConcurrencyException:
                {
                    var exceptionHandler = serviceProvider.GetService<DbUpdateConcurrencyExceptionHandler>();
                    result = exceptionHandler.Handle(dbUpdateConcurrencyException);
                    break;
                }
                case BusinessException businessException:
                {
                    var exceptionHandler = serviceProvider.GetService<BusinessExceptionHandler>();
                    result = exceptionHandler.Handle(businessException);
                    break;
                }
                default:
                {
                    var exceptionHandler = serviceProvider.GetService<UnhandledExceptionHandler>();
                    result = exceptionHandler.Handle(exception);
                    break;
                }
            }

            return result;
        }

        private static string GetLogErrorMessage(ErrorDescription errorDescription)
        {
            return
                $"Error: [{errorDescription}], SystemMessage: {errorDescription.SystemMessage}, Message: {errorDescription.Message}";
        }
    }
}