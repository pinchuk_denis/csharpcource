﻿namespace Infrastructure
{
    public interface IEntityKey<TId>
    {
        TId Id { get; set; }
    }
}