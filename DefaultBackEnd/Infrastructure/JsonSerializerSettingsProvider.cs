﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Infrastructure
{
    public class JsonSerializerSettingsProvider
    {
        public static JsonSerializerSettings GetDefaultSettings()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = GetDefaultContractResolver(),
                Converters = new List<JsonConverter>
                {
                    GetDefaultConverter()
                },
                ReferenceLoopHandling = GetDefaultReferenceLoopHandling()
            };
        }

        public static JsonSerializerSettings GetDefaultSettingsWithIgnoreNullValues()
        {
            return new JsonSerializerSettings
            {
                ContractResolver = GetDefaultContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new List<JsonConverter>
                {
                    GetDefaultConverter()
                },
                ReferenceLoopHandling = GetDefaultReferenceLoopHandling()
            };
        }

        public static ReferenceLoopHandling GetDefaultReferenceLoopHandling()
        {
            return ReferenceLoopHandling.Ignore;
        }

        public static StringEnumConverter GetDefaultConverter()
        {
            return new StringEnumConverter(new DefaultNamingStrategy(), false);
        }

        public static CamelCasePropertyNamesContractResolver GetDefaultContractResolver()
        {
            return new CamelCasePropertyNamesContractResolver();
        }
    }
}