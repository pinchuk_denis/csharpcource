﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace Infrastructure.HttpResponse
{
    public class HttpResponseWriter
    {
        public virtual async Task WriteAsync(
            Microsoft.AspNetCore.Http.HttpResponse httpResponse,
            HttpStatusCode statusCode,
            string contentType,
            string bodyContent)
        {
            httpResponse.StatusCode = (int) statusCode;
            httpResponse.ContentType = contentType;
            httpResponse.Headers[HeaderNames.CacheControl] = "no-cache";
            httpResponse.Headers[HeaderNames.Pragma] = "no-cache";
            httpResponse.Headers[HeaderNames.Expires] = "-1";
            httpResponse.Headers.Remove(HeaderNames.ETag);

            await httpResponse.WriteAsync(bodyContent);
        }
    }
}