using System;
using System.Diagnostics;
using System.Linq;

namespace Infrastructure.SystemInformation
{
    public class ProcessExecutionMonitor
    {
        private readonly Stopwatch stopwatch = new Stopwatch();

        public bool MainProcessEnded { get; private set; }

        public bool MainProcessStarted => MainProcess != null;

        public ProcessExecutionInformation MainProcess { get; private set; }

        public void StartProcess(string processName)
        {
            if (MainProcess == null)
                StartMainProcess(processName);
            else
                AddInnerProcess(processName);
        }

        public void InterruptCurrentProcess()
        {
            TerminateCurrentProcess(true);
        }

        public void EndProcessByName(string processName)
        {
            throw new NotImplementedException();
        }

        public void EndCurrentProcess()
        {
            TerminateCurrentProcess(false);
        }

        public void TerminateCurrentProcess(bool withInterruption)
        {
            if (MainProcess != null)
            {
                var notEndedStep = FindNotEndedInnerProcess(MainProcess);

                if (notEndedStep != null)
                {
                    TerminateCurrentProcess(notEndedStep, withInterruption);
                }
                else
                {
                    TerminateCurrentProcess(MainProcess, withInterruption);
                    stopwatch.Stop();
                    MainProcessEnded = true;
                }
            }
        }

        private void StartMainProcess(string processName)
        {
            stopwatch.Restart();
            MainProcess = StartNewProcess(processName);
            MainProcessEnded = false;
        }

        private ProcessExecutionInformation StartNewProcess(string processName)
        {
            return new ProcessExecutionInformation(processName, stopwatch.ElapsedMilliseconds);
        }

        private void AddInnerProcess(string processName)
        {
            var notEndedInnerProcess = FindNotEndedInnerProcess(MainProcess) ?? MainProcess;

            notEndedInnerProcess.AddInnerActionInfo(StartNewProcess(processName));
        }

        private void TerminateCurrentProcess(ProcessExecutionInformation process, bool withInterruption)
        {
            if (withInterruption)
                process.InterruptExecution(stopwatch.ElapsedMilliseconds);
            else
                process.EndExecution(stopwatch.ElapsedMilliseconds);
        }

        private ProcessExecutionInformation FindNotEndedInnerProcess(ProcessExecutionInformation process)
        {
            var result = process.InnerProcesses.LastOrDefault(x => !x.IsEnded);

            if (result != null && result.InnerProcesses.Any())
            {
                var notEndedInnerProcess = FindNotEndedInnerProcess(result);

                if (notEndedInnerProcess != null) result = notEndedInnerProcess;
            }

            return result;
        }
    }
}