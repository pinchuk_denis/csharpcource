using System.Collections.Generic;
using Newtonsoft.Json;

namespace Infrastructure.SystemInformation
{
    public class ProcessExecutionInformation
    {
        private readonly long startAtMilliseconds;

        private long? endsAtMilliseconds;

        public ProcessExecutionInformation(string name, long startAtMilliseconds)
        {
            Name = name;
            this.startAtMilliseconds = startAtMilliseconds;
        }

        public ICollection<ProcessExecutionInformation> InnerProcesses { get; } =
            new List<ProcessExecutionInformation>();

        public string Name { get; }

        public long DurationMilliseconds => IsEnded ? endsAtMilliseconds.Value - startAtMilliseconds : 0;

        [JsonIgnore] public bool IsEnded => endsAtMilliseconds.HasValue;

        public bool IsInterrupted { get; private set; }

        public void EndExecution(long endsAt)
        {
            endsAtMilliseconds = endsAt;
        }

        public void InterruptExecution(long endsAt)
        {
            IsInterrupted = true;

            EndExecution(endsAt);
        }

        public void AddInnerActionInfo(ProcessExecutionInformation step)
        {
            InnerProcesses.Add(step);
        }
    }
}