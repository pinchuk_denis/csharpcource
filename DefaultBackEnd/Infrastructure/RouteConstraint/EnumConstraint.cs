﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Infrastructure.RouteConstraint
{
    public class EnumConstraint : IRouteConstraint
    {
        private static readonly ConcurrentDictionary<string, string[]> Cache =
            new ConcurrentDictionary<string, string[]>();

        private readonly IEnumerable<string> validOptions;

        public EnumConstraint(string enumType, string assemblyName)
        {
            validOptions = Cache.GetOrAdd($"{enumType}, {assemblyName}", key =>
            {
                var type = Type.GetType(key);
                return type != null ? Enum.GetNames(type) : new string[0];
            });
        }

        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            if (values.TryGetValue(routeKey, out var value) && value != null)
                return validOptions.Contains(value.ToString(), StringComparer.OrdinalIgnoreCase);

            return false;
        }
    }
}