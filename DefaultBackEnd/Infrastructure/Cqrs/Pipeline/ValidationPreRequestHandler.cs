﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Infrastructure.Exceptions;
using Infrastructure.Validation;
using MediatR.Pipeline;

namespace Infrastructure.Cqrs.Pipeline
{
    public class ValidationPreRequestHandler<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly IValidator<TRequest> validator;

        public ValidationPreRequestHandler(IValidator<TRequest> validator)
        {
            this.validator = validator;
        }

        public async Task Process(TRequest request, CancellationToken cancellationToken)
        {
            var validationResult = await validator.ValidateAsync(request, cancellationToken);

            if (!validationResult.IsValid)
            {
                if (validator is FluentValidator<TRequest> fluentValidator)
                    throw new FluentValidationException(
                        "Validation Error",
                        validationResult.Errors,
                        fluentValidator.LocalizationResourcesType);

                throw new ValidationException("Validation Error", validationResult.Errors);
            }
        }
    }
}