﻿using MediatR;

namespace Infrastructure.Cqrs.Commands
{
    public interface ICommand : IRequest<Unit>
    {
    }

    public interface ICommand<out TResponse> : IRequest<TResponse>
    {
    }
}