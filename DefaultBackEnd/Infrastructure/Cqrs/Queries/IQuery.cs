﻿using MediatR;

namespace Infrastructure.Cqrs.Queries
{
    public interface IQuery
    {
    }

    public interface IQuery<out TResult> : IRequest<TResult>, IQuery
    {
    }
}