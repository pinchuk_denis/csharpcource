﻿using System;

namespace Infrastructure.Localization.Enum
{
    public class EnumLocalizer
    {
        private readonly ResourceStringLocalizer localizer;

        public EnumLocalizer(ResourceStringLocalizer localizer)
        {
            this.localizer = localizer;
        }

        public TEnumSummary Localize<TEnum, TEnumSummary>(
            TEnum enumValue,
            Type localizationResourcesType)
            where TEnum : System.Enum
            where TEnumSummary : ILocalizedEnumEntity<TEnum>, new()
        {
            var enumType = typeof(TEnum);

            if (!System.Enum.IsDefined(enumType, enumValue))
                throw new ArgumentOutOfRangeException(
                    nameof(enumType),
                    $"{nameof(enumType)} '{enumValue}' not supported");

            return new TEnumSummary
            {
                Id = enumValue,
                Name = localizer.Localize($"{enumType.Name}_{enumValue}", localizationResourcesType)
            };
        }

        public TEnumSummary Localize<TEnum, TEnumSummary>(
            int enumValue,
            Type localizationResourcesType)
            where TEnum : System.Enum
            where TEnumSummary : ILocalizedEnumEntity<TEnum>, new()
        {
            return Localize<TEnum, TEnumSummary>(
                (TEnum) System.Enum.ToObject(typeof(TEnum), enumValue),
                localizationResourcesType);
        }
    }
}