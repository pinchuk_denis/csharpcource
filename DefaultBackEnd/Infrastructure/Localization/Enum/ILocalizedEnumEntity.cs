﻿namespace Infrastructure.Localization.Enum
{
    public interface ILocalizedEnumEntity<TEnum> : IEntitySummary<TEnum> where TEnum : System.Enum
    {
    }
}