﻿namespace Infrastructure.Localization.Enum
{
    public class LocalizedEnumEntity<TEnum> : ILocalizedEnumEntity<TEnum> where TEnum : System.Enum
    {
        public TEnum Id { get; set; }

        public string Name { get; set; }
    }
}