﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Localization.Enum
{
    public class EnumLocalizedAvailableValuesProvider
    {
        private readonly EnumLocalizer enumLocalizer;

        public EnumLocalizedAvailableValuesProvider(EnumLocalizer enumLocalizer)
        {
            this.enumLocalizer = enumLocalizer;
        }

        public List<TEnumTypeSummary> GetAvailableValues<TEnumType, TEnumTypeSummary>(Type localizationResourceType)
            where TEnumType : System.Enum
            where TEnumTypeSummary : ILocalizedEnumEntity<TEnumType>, new()
        {
            var result = new List<TEnumTypeSummary>();

            foreach (var enumItem in EnumUtilities.AvailableValues<TEnumType>())
                result.Add(
                    enumLocalizer.Localize<TEnumType, TEnumTypeSummary>(
                        enumItem,
                        localizationResourceType));

            return result;
        }
    }
}