﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using SmartFormat;

namespace Infrastructure.Localization
{
    public class ResourceStringLocalizer
    {
        private readonly ILogger<ResourceStringLocalizer> logger;

        private readonly IStringLocalizerFactory stringLocalizerFactory;

        public ResourceStringLocalizer(IStringLocalizerFactory stringLocalizerFactory,
            ILogger<ResourceStringLocalizer> logger)
        {
            this.stringLocalizerFactory = stringLocalizerFactory;
            this.logger = logger;
        }

        public virtual string Localize(string resourceKey, Type resourcesType)
        {
            return Localize(resourceKey, resourcesType, resourceKey);
        }

        public virtual string Localize(string resourceKey, Type resourcesType, string defaultValue)
        {
            return Localize(resourceKey, resourcesType, defaultValue, null);
        }

        public virtual string Localize(
            string resourceKey,
            Type resourcesType,
            string defaultValue,
            Dictionary<string, object> formattedMessagePlaceholderValues)
        {
            var result = defaultValue;

            if (resourcesType != null)
            {
                var localizer = stringLocalizerFactory.Create(resourcesType);

                var localizedString = localizer[resourceKey];
                if (localizedString.ResourceNotFound)
                    logger.LogWarning(
                        $"Localized value for key {resourceKey} not found in resource file {resourcesType}");
                else
                    result = FormatLocalizedMessage(localizedString, formattedMessagePlaceholderValues);
            }
            else
            {
                logger.LogError($"Localizer not found for resource type {resourcesType}");
            }

            return result;
        }

        private string FormatLocalizedMessage(
            string sourceErrorMessage,
            Dictionary<string, object> formattedMessagePlaceholderValues)
        {
            return formattedMessagePlaceholderValues != null
                ? Smart.Format(sourceErrorMessage, formattedMessagePlaceholderValues)
                : sourceErrorMessage;
        }
    }
}