﻿using System;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace Infrastructure
{
    public static class ConfigurationExtensions
    {
        /// <summary>
        ///     Extracts the required value with specified <paramref name="key" /> and converts it to type T.
        /// </summary>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <param name="configuration">The configuration.</param>
        /// <param name="key">The key of configuration section's to convert.</param>
        /// <returns>The value converted to type T</returns>
        /// <exception cref="ConfigurationErrorsException">If value not found in configuration section.</exception>
        public static T GetRequiredValue<T>(this IConfiguration configuration, string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = configuration.GetValue<T>(key);

            if (value == null) throw new ConfigurationErrorsException(key);

            return value;
        }

        /// <summary>
        ///     Gets a required configuration section with the specified <paramref name="key" />.
        /// </summary>
        /// <param name="configuration">The configuration section.</param>
        /// <param name="key">The key of configuration section.</param>
        /// <returns>The configuration section</returns>
        /// <exception cref="ConfigurationErrorsException">If configuration section not found.</exception>
        public static IConfigurationSection GetRequiredSection(this IConfiguration configuration, string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var result = configuration.GetSection(key);

            if (result == null) throw new ConfigurationErrorsException(key);

            return result;
        }

        /// <summary>
        ///     Extracts the required value with specified <paramref name="key" /> and converts it to type T.
        /// </summary>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <param name="configurationSection">The configuration.</param>
        /// <param name="key">The key of configuration section's to convert.</param>
        /// <returns>The value converted to type T</returns>
        /// <exception cref="ConfigurationErrorsException">If value not found in configuration section.</exception>
        public static T GetRequiredValue<T>(this IConfigurationSection configurationSection, string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            var value = configurationSection.GetValue<T>(key);

            if (value == null) throw new ConfigurationErrorsException($"{configurationSection.Key}:{key}");

            return value;
        }
    }
}