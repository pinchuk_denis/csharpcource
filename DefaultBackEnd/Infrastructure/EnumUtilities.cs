using System;
using System.Collections.Generic;

namespace Infrastructure
{
    public static class EnumUtilities
    {
        public static ICollection<TEnum> AvailableValues<TEnum>()
            where TEnum : Enum
        {
            var result = new List<TEnum>();

            foreach (TEnum enumItem in Enum.GetValues(typeof(TEnum))) result.Add(enumItem);

            return result;
        }
    }
}