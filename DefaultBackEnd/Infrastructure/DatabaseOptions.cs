namespace Infrastructure
{
    public class DatabaseOptions
    {
        public DatabaseOptions()
        {
        }

        public DatabaseOptions(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public DatabaseOptions(string connectionString, int commandTimeoutSeconds)
        {
            ConnectionString = connectionString;
            CommandTimeoutSeconds = commandTimeoutSeconds;
        }

        public string ConnectionString { get; set; }

        public int CommandTimeoutSeconds { get; set; } = 30;
    }
}