﻿namespace Infrastructure.Options
{
    public class HttpResponseOptions
    {
        public bool ReturnErrorMessageWithDebugInformation { get; set; }

        public bool ReturnRequestExecutionDetails { get; set; }
    }
}