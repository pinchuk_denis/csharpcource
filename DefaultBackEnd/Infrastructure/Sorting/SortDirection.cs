﻿namespace Infrastructure.Sorting
{
    public enum SortDirection
    {
        Asc,
        Desc
    }
}