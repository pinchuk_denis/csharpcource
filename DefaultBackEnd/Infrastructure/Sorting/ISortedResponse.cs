﻿namespace Infrastructure.Sorting
{
    public interface ISortedResponse<TSortingField>
    {
        SortingRule<TSortingField>[] Sorting { get; set; }
    }
}