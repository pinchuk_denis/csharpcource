namespace Infrastructure.Sorting
{
    public class SortingRule
    {
        public string PropertyName { get; set; }

        public SortDirection SortDirection { get; set; }
    }

    public class SortingRule<TPropertyName>
    {
        public TPropertyName PropertyName { get; set; }

        public SortDirection SortDirection { get; set; }
    }
}