﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Infrastructure.Sorting
{
    public static class ApplyOrderingExtensions
    {
        public static IOrderedQueryable<T> ApplyOrdering<T>(this IQueryable<T> source, IList<SortingRule> sortingRules)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            if (sortingRules == null) throw new ArgumentNullException(nameof(sortingRules));

            if (!sortingRules.Any())
                throw new ArgumentException($"{nameof(sortingRules)} must contain at least one sorting rule.");

            var result = source.OrderBy($"{sortingRules[0].PropertyName} {sortingRules[0].SortDirection}");

            for (var i = 1; i < sortingRules.Count; i++)
                result = result.ThenBy($"{sortingRules[i].PropertyName} {sortingRules[i].SortDirection}");

            return result;
        }

        public static IQueryable<TEntity> ApplyOrdering<TEntity, TSortingField>(
            this IQueryable<TEntity> source,
            ISortedRequest<TSortingField> request,
            SortingRules<TEntity, TSortingField> sortingRules)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));

            if (request == null) throw new ArgumentNullException(nameof(request));

            if (request.Sorting == null || !request.Sorting.Any()) request.Sorting = sortingRules.DefaultSortingRules;

            sortingRules.AddMandatorySortingRule(request);

            IOrderedQueryable<TEntity> result = null;

            if (sortingRules.SortingExpressions.TryGetValue(request.Sorting[0].PropertyName, out var orderBy))
                result = request.Sorting[0].SortDirection == SortDirection.Asc
                    ? source.OrderBy(orderBy)
                    : source.OrderByDescending(orderBy);

            for (var i = 1; i < request.Sorting.Length; i++)
                if (result != null &&
                    sortingRules.SortingExpressions.TryGetValue(request.Sorting[i].PropertyName, out orderBy))
                    result = request.Sorting[i].SortDirection == SortDirection.Asc
                        ? result.ThenBy(orderBy)
                        : result.ThenByDescending(orderBy);

            return result ?? source;
        }
    }
}