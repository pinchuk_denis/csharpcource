﻿namespace Infrastructure.Sorting
{
    public interface ISortedRequest<TSortingField>
    {
        SortingRule<TSortingField>[] Sorting { get; set; }
    }
}