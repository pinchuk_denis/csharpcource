﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Sorting
{
    public abstract class SortingRules<TEntity, TSortingField> : ISortingRules
    {
        public abstract SortingRule<TSortingField>[] DefaultSortingRules { get; }

        public abstract IReadOnlyDictionary<TSortingField, Expression<Func<TEntity, object>>> SortingExpressions
        {
            get;
        }

        public virtual void AddMandatorySortingRule(ISortedRequest<TSortingField> request)
        {
            var mandatorySortingRule = DefaultSortingRules.Last();

            var isMandatorySortingRuleAdded = false;
            foreach (var sortingRule in request.Sorting)
            {
                isMandatorySortingRuleAdded = IsMandatorySortingRuleAdded(sortingRule, mandatorySortingRule);
                if (isMandatorySortingRuleAdded) break;
            }

            if (!isMandatorySortingRuleAdded) request.Sorting = request.Sorting.Append(mandatorySortingRule).ToArray();
        }

        protected virtual bool IsMandatorySortingRuleAdded(SortingRule<TSortingField> sortingRuleToCompare,
            SortingRule<TSortingField> mandatorySortingRule)
        {
            return sortingRuleToCompare.PropertyName.Equals(mandatorySortingRule.PropertyName);
        }
    }
}