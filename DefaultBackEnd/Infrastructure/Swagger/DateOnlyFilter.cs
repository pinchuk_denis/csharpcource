﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Infrastructure.Swagger
{
    public class DateOnlyFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model.Properties?.Keys == null) return;

            var requiredProperties = context.Type.GetProperties()
                .Where(prop => (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                               && prop.GetCustomAttributes<DateOnlyAttribute>().Any());

            foreach (var property in requiredProperties)
            {
                var camelCasePropertyName = PropertyNameConverter.ConvertToCamelCase(property.Name);
                if (model.Properties.ContainsKey(camelCasePropertyName))
                    model.Properties[camelCasePropertyName].Format = "date";
            }
        }
    }
}