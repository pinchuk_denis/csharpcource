﻿using System;

namespace Infrastructure.Swagger
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DateOnlyAttribute : Attribute
    {
    }
}