﻿using System;

namespace Infrastructure.Swagger
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyRequiredAttribute : Attribute
    {
    }
}