﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Infrastructure.Swagger
{
    public class PropertyRequiredFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model.Required == null) model.Required = new HashSet<string>();

            if (model.Properties?.Keys == null) return;

            var requiredProperties = context.Type.GetProperties()
                .Where(prop => prop.GetCustomAttributes<PropertyRequiredAttribute>().Any());

            foreach (var property in requiredProperties)
            {
                var camelCasePropertyName = PropertyNameConverter.ConvertToCamelCase(property.Name);
                if (model.Properties.ContainsKey(camelCasePropertyName)) model.Required.Add(camelCasePropertyName);
            }
        }
    }
}