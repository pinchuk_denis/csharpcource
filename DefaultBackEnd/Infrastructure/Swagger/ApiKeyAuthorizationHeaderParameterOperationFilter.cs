﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Infrastructure.Swagger
{
    public class ApiKeyAuthorizationHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var filterPipeline = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            bool allowAnonymous = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is IAllowAnonymousFilter);
            bool isAuthorizedByApiKeyRequired = filterPipeline.Select(filterInfo => filterInfo.Filter)
                .Any(filter => typeof(IAuthorizeByApiKeyFilter).IsAssignableFrom(this.GetFilterImplementationType(filter)));


            if (!allowAnonymous && isAuthorizedByApiKeyRequired)
            {
                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "x-api-key",
                    In = ParameterLocation.Header,
                    Required = true,
                    Schema = new OpenApiSchema
                    {
                        Type = "string",
                        Default = new OpenApiString("your_x-api-key")
                    }
                });
            }
        }

        private Type GetFilterImplementationType(object filter)
        {
            var bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            var property = filter.GetType().GetProperty("ImplementationType", bindFlags);
            if (property == null)
            {
                return null;
            }

            return property.GetValue(filter) as Type;
        }
    }
}
