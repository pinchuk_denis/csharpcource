﻿namespace Infrastructure.Swagger
{
    public class PropertyNameConverter
    {
        public static string ConvertToCamelCase(string inputString)
        {
            if (inputString == null) return null;

            return char.ToLowerInvariant(inputString[0]) + inputString.Substring(1);
        }
    }
}