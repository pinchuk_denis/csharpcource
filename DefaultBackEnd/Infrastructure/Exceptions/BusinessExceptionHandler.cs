﻿using System.Collections.Generic;
using System.Net;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Exceptions
{
    public class BusinessExceptionHandler : ExceptionHandler<BusinessException>
    {
        public BusinessExceptionHandler(IStringLocalizerFactory stringLocalizerFactory)
            : base(stringLocalizerFactory)
        {
        }

        protected override HttpStatusCode GetHttpStatusCode(BusinessException exception)
        {
            var result = HttpStatusCode.BadRequest;

            if (exception is NotFoundException)
                result = HttpStatusCode.NotFound;
            else if (exception is UnauthorizedException)
                result = HttpStatusCode.Unauthorized;
            else if (exception is ForbiddenException) result = HttpStatusCode.Forbidden;

            return result;
        }

        protected override void FillResponseWithErrors(ErrorResponse errorResponse, BusinessException exception,
            IStringLocalizer stringLocalizer)
        {
            errorResponse.Errors = new List<ErrorDescription>
            {
                new ErrorDescription
                {
                    Key = exception.Error.Key,
                    Code = exception.Error.Code,
                    SystemMessage = exception.Error.SystemMessage,
                    Message = LocalizeErrorMessage(
                        exception.Error.Code,
                        exception.Error.Message,
                        stringLocalizer,
                        exception.FormattedMessagePlaceholderValues)
                }
            };
        }

        protected override IStringLocalizer GetStringLocalizer(BusinessException exception)
        {
            IStringLocalizer localizer = null;

            if (exception.LocalizationResourcesType != null)
                localizer = StringLocalizerFactory.Create(exception.LocalizationResourcesType);

            return localizer;
        }
    }
}