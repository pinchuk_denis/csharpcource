﻿using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;

namespace Infrastructure.Exceptions
{
    public class ModelBindingException : ValidationException
    {
        public ModelBindingException(string message) : base(message)
        {
        }

        public ModelBindingException(string message, IEnumerable<ValidationFailure> errors) : base(message, errors)
        {
        }

        public ModelBindingException(IEnumerable<ValidationFailure> errors) : base(errors)
        {
        }
    }
}