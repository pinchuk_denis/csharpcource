﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Exceptions
{
    public class DbUpdateConcurrencyExceptionHandler : ExceptionHandler<DbUpdateConcurrencyException>
    {
        public DbUpdateConcurrencyExceptionHandler(
            IStringLocalizerFactory stringLocalizerFactory)
            : base(stringLocalizerFactory)
        {
        }

        protected override HttpStatusCode GetHttpStatusCode(DbUpdateConcurrencyException exception)
        {
            return HttpStatusCode.Conflict;
        }

        protected override void FillResponseWithErrors(ErrorResponse errorResponse,
            DbUpdateConcurrencyException exception, IStringLocalizer stringLocalizer)
        {
            errorResponse.Errors = new List<ErrorDescription>
            {
                new ErrorDescription
                {
                    Key = ErrorKeys.RowVersionError,
                    Code = "Error_Concurrency_Exception",
                    Message = "Entity was modified by another user. Please refresh the page.",
                    SystemMessage = $"{exception.Message} ({EntriesUpdateInfo(exception)}) {exception.StackTrace}"
                }
            };
        }

        private static string EntriesUpdateInfo(DbUpdateConcurrencyException exception)
        {
            return exception.Entries.Aggregate(string.Empty,
                (current, entry) =>
                    current +
                    $"Entry: {entry.Entity.GetType()}, PrimaryKey: {EntryPrimaryKeyInfo(entry)}, KeyState: {entry.State}");
        }

        private static string EntryPrimaryKeyInfo(EntityEntry entry)
        {
            var primaryKeyProperties = entry.OriginalValues.Properties.Where(x => x.IsPrimaryKey());

            return string.Join(",",
                primaryKeyProperties.Select(x => $"{x.Name}={x.PropertyInfo.GetValue(entry.Entity)}").ToArray());
        }
    }
}