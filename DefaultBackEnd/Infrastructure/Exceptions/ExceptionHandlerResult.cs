﻿using System.Net;

namespace Infrastructure.Exceptions
{
    public class ExceptionHandlerResult
    {
        public HttpStatusCode HttpStatusCode { get; set; }

        public ErrorResponse ErrorResponse { get; set; }
    }
}