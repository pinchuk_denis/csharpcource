﻿namespace Infrastructure.Exceptions
{
    public abstract class NotFoundException : BusinessException
    {
        protected NotFoundException(ErrorDescription error)
            : base(error)
        {
        }
    }
}