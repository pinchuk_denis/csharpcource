﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infrastructure.Exceptions
{
    [XmlRoot("errorResponse")]
    public class ErrorResponse
    {
        [XmlElement("referenceId")] public Guid ReferenceId { get; set; }

        [XmlArray("errors")]
        [XmlArrayItem("error")]
        public List<ErrorDescription> Errors { get; set; }
    }
}