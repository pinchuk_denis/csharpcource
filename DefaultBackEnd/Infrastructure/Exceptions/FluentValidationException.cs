﻿using System;
using System.Collections.Generic;
using FluentValidation;
using FluentValidation.Results;

namespace Infrastructure.Exceptions
{
    public class FluentValidationException : ValidationException
    {
        public FluentValidationException(
            string message,
            IEnumerable<ValidationFailure> errors,
            Type localizationResourcesType)
            : base(message, errors)
        {
            LocalizationResourcesType = localizationResourcesType;
        }

        public Type LocalizationResourcesType { get; }
    }
}