﻿using System;
using System.Threading.Tasks;
using Infrastructure.HttpResponse;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Infrastructure.Exceptions
{
    public class ExceptionResponseHandler : IExceptionResponseHandler
    {
        private readonly HttpResponseWriter httpResponseWriter;

        public ExceptionResponseHandler(HttpResponseWriter httpResponseWriter)
        {
            this.httpResponseWriter = httpResponseWriter;
        }

        public async Task HandleAsync(ExceptionHandlerResult exceptionHandlerResult, HttpContext httpContext,
            Exception exception)
        {
            var bodyContent = JsonConvert.SerializeObject(
                exceptionHandlerResult.ErrorResponse,
                JsonSerializerSettingsProvider.GetDefaultSettings());

            await httpResponseWriter.WriteAsync(
                httpContext.Response,
                exceptionHandlerResult.HttpStatusCode,
                "application/json;charset=UTF-8",
                bodyContent);
        }
    }
}