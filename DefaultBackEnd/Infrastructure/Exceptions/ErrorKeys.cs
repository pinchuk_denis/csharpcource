﻿namespace Infrastructure.Exceptions
{
    public class ErrorKeys
    {
        public const string RequestError = "Request_Error";
        public const string RowVersionError = "RowVersion_Error";
    }
}