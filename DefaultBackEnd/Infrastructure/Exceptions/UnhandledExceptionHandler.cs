﻿using System;
using System.Collections.Generic;
using System.Net;
using Infrastructure.Options;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace Infrastructure.Exceptions
{
    public class UnhandledExceptionHandler : ExceptionHandler<Exception>
    {
        private readonly HttpResponseOptions httpResponseOptions;

        public UnhandledExceptionHandler(
            IOptions<HttpResponseOptions> httpResponseOptions,
            IStringLocalizerFactory stringLocalizerFactory)
            : base(stringLocalizerFactory)
        {
            this.httpResponseOptions = httpResponseOptions.Value;
        }

        protected override HttpStatusCode GetHttpStatusCode(Exception exception)
        {
            return HttpStatusCode.InternalServerError;
        }

        protected override void FillResponseWithErrors(ErrorResponse errorResponse, Exception exception,
            IStringLocalizer stringLocalizer)
        {
            errorResponse.Errors = new List<ErrorDescription>
            {
                new ErrorDescription
                {
                    Key = HttpStatusCode.InternalServerError.ToString(),
                    Code = HttpStatusCode.InternalServerError.ToString(),
                    Message =
                        httpResponseOptions.ReturnErrorMessageWithDebugInformation
                            ? $"{exception.Message} {exception.StackTrace}"
                            : "Internal server error",
                    SystemMessage = $"{exception.Message} {exception.StackTrace}"
                }
            };
        }
    }
}