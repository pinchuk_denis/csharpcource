﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation;
using FluentValidation.Results;
using Infrastructure.Validation;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Exceptions
{
    public class ValidationExceptionHandler : ExceptionHandler<ValidationException>
    {
        private readonly Dictionary<Type, IStringLocalizer> сustomStingLocalizers =
            new Dictionary<Type, IStringLocalizer>();

        public ValidationExceptionHandler(IStringLocalizerFactory stringLocalizerFactory)
            : base(stringLocalizerFactory)
        {
        }

        protected override HttpStatusCode GetHttpStatusCode(ValidationException exception)
        {
            return HttpStatusCode.BadRequest;
        }

        protected override void FillResponseWithErrors(
            ErrorResponse errorResponse,
            ValidationException exception,
            IStringLocalizer stringLocalizer)
        {
            var errors = exception.Errors.ToList();

            errorResponse.Errors = new List<ErrorDescription>(errors.Count);

            foreach (var error in errors)
            {
                var message = LocalizeErrorMessage(error, stringLocalizer);

                errorResponse.Errors.Add(new ErrorDescription
                {
                    Key = DefineErrorKey(error),
                    Code = error.ErrorCode,
                    Message = message
                });
            }
        }

        protected override IStringLocalizer GetStringLocalizer(ValidationException exception)
        {
            IStringLocalizer result = null;

            if (exception is FluentValidationException fluentValidationException
                && fluentValidationException.LocalizationResourcesType != null)
                result = GetStringLocalizer(fluentValidationException.LocalizationResourcesType);

            return result;
        }

        private static string DefineErrorKey(ValidationFailure validationFailure)
        {
            var result = validationFailure.PropertyName;

            if (validationFailure.CustomState != null
                && validationFailure.CustomState is ValidationFailureCustomState validationFailureCustomState
                && validationFailureCustomState.OverridenPropertyName != null)
                result = validationFailureCustomState.OverridenPropertyName.Value;

            return result;
        }

        private IStringLocalizer GetCustomStringLocalizer(Type value)
        {
            IStringLocalizer result;

            if (сustomStingLocalizers.ContainsKey(value))
            {
                result = сustomStingLocalizers[value];
            }
            else
            {
                result = GetStringLocalizer(value);
                сustomStingLocalizers[value] = result;
            }

            return result;
        }

        private string LocalizeErrorMessage(ValidationFailure validationFailure, IStringLocalizer stringLocalizer)
        {
            var resultStringLocalizer = stringLocalizer;

            if (validationFailure.CustomState != null
                && validationFailure.CustomState is ValidationFailureCustomState validationFailureCustomState
                && validationFailureCustomState.OverridenLocalizationResourcesType != null)
                resultStringLocalizer =
                    GetCustomStringLocalizer(validationFailureCustomState.OverridenLocalizationResourcesType.Value);

            return LocalizeErrorMessage(
                validationFailure.ErrorCode,
                validationFailure.ErrorCode,
                resultStringLocalizer,
                validationFailure.FormattedMessagePlaceholderValues);
        }

        private IStringLocalizer GetStringLocalizer(Type localizationResourceType)
        {
            return StringLocalizerFactory.Create(localizationResourceType);
        }
    }
}