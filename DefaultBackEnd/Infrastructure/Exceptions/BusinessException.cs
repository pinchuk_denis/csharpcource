﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Exceptions
{
    public abstract class BusinessException : Exception
    {
        protected BusinessException(ErrorDescription error)
            : base(error.Message)
        {
            Error = error;
        }

        public abstract Type LocalizationResourcesType { get; }

        public ErrorDescription Error { get; }

        public Dictionary<string, object> FormattedMessagePlaceholderValues { get; protected set; }
    }
}