﻿using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Infrastructure.Exceptions
{
    public class ErrorDescription
    {
        [XmlElement("key")] public string Key { get; set; }

        [XmlElement("code")] public string Code { get; set; }

        [XmlElement("message")] public string Message { get; set; }

        [XmlIgnore] [JsonIgnore] public string SystemMessage { get; set; }

        public override string ToString()
        {
            return $"Key: {Key}, Code: {Code}, Message: {Message}";
        }
    }
}