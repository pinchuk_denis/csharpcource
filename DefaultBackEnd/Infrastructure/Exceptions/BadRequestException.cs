﻿namespace Infrastructure.Exceptions
{
    public abstract class BadRequestException : BusinessException
    {
        protected BadRequestException(ErrorDescription error)
            : base(error)
        {
        }
    }
}