﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Extensions.Localization;
using SmartFormat;

namespace Infrastructure.Exceptions
{
    public abstract class ExceptionHandler<TException>
        where TException : Exception
    {
        protected ExceptionHandler(IStringLocalizerFactory stringLocalizerFactory)
        {
            StringLocalizerFactory = stringLocalizerFactory;
        }

        protected IStringLocalizerFactory StringLocalizerFactory { get; }

        public ExceptionHandlerResult Handle(TException exception)
        {
            var httpStatusCode = GetHttpStatusCode(exception);

            var errorResponse = GetErrorResponse(exception);

            var result = new ExceptionHandlerResult
            {
                HttpStatusCode = httpStatusCode,
                ErrorResponse = errorResponse
            };

            return result;
        }

        protected abstract HttpStatusCode GetHttpStatusCode(TException exception);

        protected abstract void FillResponseWithErrors(ErrorResponse errorResponse, TException exception,
            IStringLocalizer stringLocalizer);

        protected virtual ErrorResponse GetErrorResponse(TException exception)
        {
            var errorResponse = new ErrorResponse {ReferenceId = Guid.NewGuid()};

            var stringLocalizer = GetStringLocalizer(exception);

            FillResponseWithErrors(errorResponse, exception, stringLocalizer);

            return errorResponse;
        }

        protected virtual IStringLocalizer GetStringLocalizer(TException exception)
        {
            return null;
        }

        protected string LocalizeErrorMessage(
            string errorCode,
            string defaultMessage,
            IStringLocalizer stringLocalizer,
            Dictionary<string, object> formattedMessagePlaceholderValues)
        {
            var localizedErrorMessage = stringLocalizer != null
                ? stringLocalizer[errorCode]
                : defaultMessage;

            var result = FormatErrorMessage(localizedErrorMessage, formattedMessagePlaceholderValues);

            return result;
        }

        private string FormatErrorMessage(
            string sourceErrorMessage,
            Dictionary<string, object> formattedMessagePlaceholderValues)
        {
            return formattedMessagePlaceholderValues != null
                ? Smart.Format(sourceErrorMessage, formattedMessagePlaceholderValues)
                : sourceErrorMessage;
        }
    }
}