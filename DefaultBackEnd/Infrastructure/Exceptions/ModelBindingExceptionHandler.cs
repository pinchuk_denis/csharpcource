﻿using System.Linq;
using System.Net;
using FluentValidation;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Exceptions
{
    public class ModelBindingExceptionHandler : ExceptionHandler<ValidationException>
    {
        public ModelBindingExceptionHandler(IStringLocalizerFactory stringLocalizerFactory)
            : base(stringLocalizerFactory)
        {
        }

        protected override HttpStatusCode GetHttpStatusCode(ValidationException exception)
        {
            return HttpStatusCode.BadRequest;
        }

        protected override void FillResponseWithErrors(ErrorResponse errorResponse, ValidationException exception,
            IStringLocalizer stringLocalizer)
        {
            var errors = exception.Errors.ToList();

            errorResponse.Errors = errors.Select(x => new ErrorDescription
            {
                Key = x.PropertyName,
                Code = x.ErrorCode,
                Message = x.ErrorMessage
            }).ToList();
        }

        protected override IStringLocalizer GetStringLocalizer(ValidationException exception)
        {
            return null;
        }
    }
}