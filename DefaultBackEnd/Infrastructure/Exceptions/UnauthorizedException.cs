﻿using System;
using System.Net;

namespace Infrastructure.Exceptions
{
    public class UnauthorizedException : BusinessException
    {
        public UnauthorizedException(string message = null)
            : base(new ErrorDescription
                {Key = ErrorKeys.RequestError, Code = HttpStatusCode.Unauthorized.ToString(), Message = message})
        {
        }

        public override Type LocalizationResourcesType { get; } = null;
    }
}