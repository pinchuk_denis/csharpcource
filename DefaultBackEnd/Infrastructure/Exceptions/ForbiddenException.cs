﻿using System;
using System.Net;

namespace Infrastructure.Exceptions
{
    public class ForbiddenException : BusinessException
    {
        public ForbiddenException(ErrorDescription errorDescription)
            : base(errorDescription)
        {
        }

        public ForbiddenException()
            : this(new ErrorDescription {Key = ErrorKeys.RequestError, Code = HttpStatusCode.Forbidden.ToString()})
        {
        }

        public ForbiddenException(string errorCode, Type localizationResourcesType)
            : this(new ErrorDescription {Key = ErrorKeys.RequestError, Code = errorCode})
        {
            LocalizationResourcesType = localizationResourcesType;
        }

        public ForbiddenException(string errorMessage)
            : this(new ErrorDescription
                {Key = ErrorKeys.RequestError, Code = HttpStatusCode.Forbidden.ToString(), Message = errorMessage})
        {
        }

        public override Type LocalizationResourcesType { get; }
    }
}