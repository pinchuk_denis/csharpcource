﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Infrastructure.Exceptions
{
    public interface IExceptionResponseHandler
    {
        Task HandleAsync(ExceptionHandlerResult exceptionHandlerResult, HttpContext httpContext, Exception exception);
    }
}