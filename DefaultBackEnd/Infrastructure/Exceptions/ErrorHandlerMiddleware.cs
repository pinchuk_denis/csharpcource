﻿using System;
using System.Threading.Tasks;
using Infrastructure.Handlers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Exceptions
{
    public class ErrorHandlerMiddleware
    {
        private readonly ErrorHandler errorHandler;

        private readonly RequestDelegate next;

        public ErrorHandlerMiddleware(
            RequestDelegate next,
            ErrorHandler errorHandler)
        {
            this.next = next;
            this.errorHandler = errorHandler;
        }

        public async Task Invoke(HttpContext httpContext, IServiceProvider serviceProvider)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception exception)
            {
                // We can't do anything if the response has already started, just abort.
                if (httpContext.Response.HasStarted) throw;

                await HandleExceptionAsync(httpContext, exception, serviceProvider);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception,
            IServiceProvider serviceProvider)
        {
            var exceptionHandlerResult = errorHandler.Handle(exception, serviceProvider);

            var exceptionResponseHandler = serviceProvider.GetRequiredService<IExceptionResponseHandler>();

            await exceptionResponseHandler.HandleAsync(exceptionHandlerResult, httpContext, exception);
        }
    }
}