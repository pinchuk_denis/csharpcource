﻿namespace Infrastructure.Response
{
    public abstract class IsExistResponse : DataResponse<IsExistResult>
    {
        protected IsExistResponse(bool isExist)
            : base(new IsExistResult(isExist))
        {
        }
    }
}