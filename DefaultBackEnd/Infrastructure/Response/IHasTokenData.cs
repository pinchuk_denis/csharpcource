﻿namespace Infrastructure.Response
{
    public interface IHasTokenData
    {
        TokenData Token { get; set; }
    }
}