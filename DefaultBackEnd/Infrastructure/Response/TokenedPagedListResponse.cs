﻿using System.Collections.Generic;
using Infrastructure.Paging;

namespace Infrastructure.Response
{
    public abstract class TokenedPagedListResponse<TListItem> :
        TokenedDataResponse<ICollection<TListItem>>,
        IPagedResponse
    {
        protected TokenedPagedListResponse()
        {
        }

        protected TokenedPagedListResponse(ICollection<TListItem> list) : base(list)
        {
        }

        public PaginationResponseModel Pagination { get; set; }
    }
}