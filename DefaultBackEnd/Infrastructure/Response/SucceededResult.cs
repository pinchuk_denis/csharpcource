﻿namespace Infrastructure.Response
{
    public class SucceededResult
    {
        public SucceededResult(bool isSucceeded)
        {
            IsSucceeded = isSucceeded;
        }

        public bool IsSucceeded { get; set; }
    }
}