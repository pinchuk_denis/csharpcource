﻿using System.Collections.Generic;
using Infrastructure.Paging;
using Infrastructure.Sorting;

namespace Infrastructure.Response
{
    public abstract class TokenedPagedListWithSortingResponse<TListItem, TSortingField> :
        TokenedDataResponse<IEnumerable<TListItem>>,
        ISortedResponse<TSortingField>,
        IPagedResponse
    {
        protected TokenedPagedListWithSortingResponse()
        {
        }

        protected TokenedPagedListWithSortingResponse(IEnumerable<TListItem> list) : base(list)
        {
        }

        public PaginationResponseModel Pagination { get; set; }

        public SortingRule<TSortingField>[] Sorting { get; set; }
    }
}