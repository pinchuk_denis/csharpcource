namespace Infrastructure.Response
{
    public class IsExistResult
    {
        public IsExistResult(bool isExist)
        {
            IsExist = isExist;
        }

        public bool IsExist { get; }
    }
}