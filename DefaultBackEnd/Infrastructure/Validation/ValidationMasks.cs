﻿namespace Infrastructure.Validation
{
    public static class ValidationMasks
    {
        public const string AlphanumericMask = @"^[a-zA-Z0-9]*$";

        public const string NonAlphanumericSymbolMask = @"[^a-zA-Z0-9 -]";

        public const string NumericMask = @"^[0-9]*$";
    }
}