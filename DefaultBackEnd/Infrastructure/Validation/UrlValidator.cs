﻿using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Infrastructure.Validation
{
    public class UrlValidator<T> : PropertyValidator<T, string>
    {
        private const string Pattern =
            @"^[^\s\t\n\r\,\;]+\.[^\s\t\n\r\,\;]+$";

        public override string Name => "UrlValidator";

        public override bool IsValid(ValidationContext<T> context, string value)
        {
            return Regex.IsMatch(value, Pattern, RegexOptions.IgnoreCase);
        }
    }
}