﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;
using Infrastructure.Exceptions;

namespace Infrastructure.Validation
{
    public abstract class FluentValidator<TRequest> : AbstractValidator<TRequest>
    {
        private bool isValidationRulesSet;

        public abstract Type LocalizationResourcesType { get; }

        public override async Task<ValidationResult> ValidateAsync(
            ValidationContext<TRequest> context,
            CancellationToken cancellation = default)
        {
            var validationResult = await ValidateCoreAsync(context, cancellation);

            return await AfterValidateAsync(validationResult, context);
        }

        public async Task<ValidationResult> ValidateCoreAsync(
            ValidationContext<TRequest> context,
            CancellationToken cancellation = default)
        {
            ValidateNotNull(context.InstanceToValidate);

            await SetValidationRulesCoreAsync(context);

            return await base.ValidateAsync(context, cancellation);
        }

        protected virtual Task<ValidationResult> WithOverridenPropertyNameInCustomState(
            ValidationResult validationResult,
            ICollection<string> propertyNamesToOverride,
            string overridenPropertyName)
        {
            if (!validationResult.IsValid)
                foreach (var error in validationResult.Errors)
                    if (propertyNamesToOverride.Contains(error.PropertyName))
                        FillValidationFailureCustomState(error, overridenPropertyName);

            return Task.FromResult(validationResult);
        }

        protected virtual async Task<ValidationResult> AfterValidateAsync(ValidationResult validationResult,
            ValidationContext<TRequest> context)
        {
            return await Task.FromResult(validationResult);
        }

        protected abstract Task SetValidationRulesAsync(ValidationContext<TRequest> context);

        private static void FillValidationFailureCustomState(
            ValidationFailure error,
            string overridenPropertyName)
        {
            var overridenPropertyNameName = new OverridenPropertyName(overridenPropertyName);

            if (error.CustomState != null &&
                error.CustomState is ValidationFailureCustomState validationFailureCustomState)
                validationFailureCustomState.OverridenPropertyName = overridenPropertyNameName;
            else
                error.CustomState = new ValidationFailureCustomState
                {
                    OverridenPropertyName = overridenPropertyNameName,
                    OverridenLocalizationResourcesType = null
                };
        }

        private async Task SetValidationRulesCoreAsync(ValidationContext<TRequest> request)
        {
            if (!isValidationRulesSet)
            {
                await SetValidationRulesAsync(request);

                isValidationRulesSet = true;
            }
        }

        private void ValidateNotNull(TRequest request)
        {
            if (request == null)
                throw new FluentValidationException(
                    "Validation Error",
                    new List<ValidationFailure>
                    {
                        new ValidationFailure(null, $"{typeof(TRequest).Name} is empty", null)
                        {
                            ErrorCode = $"Error_{typeof(TRequest).Name}_Empty"
                        }
                    },
                    LocalizationResourcesType);
        }
    }
}