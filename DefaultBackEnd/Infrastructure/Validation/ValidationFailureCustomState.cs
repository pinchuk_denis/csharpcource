namespace Infrastructure.Validation
{
    public class ValidationFailureCustomState
    {
        public OverridenPropertyName OverridenPropertyName { get; set; }

        public OverridenLocalizationResourcesType OverridenLocalizationResourcesType { get; set; }
    }
}