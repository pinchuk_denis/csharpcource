﻿using System;
using System.Threading.Tasks;
using FluentValidation;

namespace Infrastructure.Validation
{
    public class EmptyFluentValidator<TRequest> : FluentValidator<TRequest>
    {
        public override Type LocalizationResourcesType => null;

        protected override Task SetValidationRulesAsync(ValidationContext<TRequest> context)
        {
            return Task.CompletedTask;
        }
    }
}