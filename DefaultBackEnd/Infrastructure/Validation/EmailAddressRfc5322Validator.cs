﻿using System.Text.RegularExpressions;
using FluentValidation;
using FluentValidation.Validators;

namespace Infrastructure.Validation
{
    public class EmailAddressRfc5322Validator<T> : PropertyValidator<T, string>
    {
        private const string Pattern =
            @"^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$";

        public override string Name => "EmailAddressRfc5322Validator";

        public override bool IsValid(ValidationContext<T> context, string value)
        {
            return value.Length <= 256
                   && Regex.IsMatch(value, Pattern, RegexOptions.IgnoreCase)
                   && value.Split("@")[0].Length <= 64;
        }
    }
}