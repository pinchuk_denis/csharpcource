﻿using System;
using FluentValidation;

namespace Infrastructure.Validation
{
    public static class FluentValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> RequiredString<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new RequiredStringValidator<T>());
        }

        public static IRuleBuilderOptions<T, string> EmailAddressRfc5322<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .SetValidator(new EmailAddressRfc5322Validator<T>());
        }

        public static IRuleBuilderOptions<T, string> Url<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .SetValidator(new UrlValidator<T>());
        }

        public static IRuleBuilderOptions<T, TProperty> WithCustomLocalizationResourcesType<T, TProperty>(
            this IRuleBuilderOptions<T, TProperty> rule,
            Type localizationResourcesType)
        {
            return rule.WithState(x => new ValidationFailureCustomState
            {
                OverridenPropertyName = null,
                OverridenLocalizationResourcesType = new OverridenLocalizationResourcesType(localizationResourcesType)
            });
        }
    }
}