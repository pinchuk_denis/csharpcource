﻿namespace Infrastructure.Validation
{
    public class OverridenPropertyName
    {
        public OverridenPropertyName(string value)
        {
            Value = value;
        }

        public string Value { get; }
    }
}