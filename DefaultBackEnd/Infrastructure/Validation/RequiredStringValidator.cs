﻿using FluentValidation;
using FluentValidation.Validators;

namespace Infrastructure.Validation
{
    public class RequiredStringValidator<T> : PropertyValidator<T, string>
    {
        public override string Name => "RequiredStringValidator";

        public override bool IsValid(ValidationContext<T> context, string value)
        {
            return !string.IsNullOrEmpty(value);
        }
    }
}