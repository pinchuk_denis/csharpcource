﻿using System.Text.RegularExpressions;

namespace Infrastructure.Validation
{
    public static class GuidValidator
    {
        private static readonly Regex GuidRegex =
            new Regex(
                @"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$",
                RegexOptions.Compiled);

        public static bool IsGuid(string candidate)
        {
            if (candidate != null)
                if (GuidRegex.IsMatch(candidate))
                    return true;

            return false;
        }
    }
}