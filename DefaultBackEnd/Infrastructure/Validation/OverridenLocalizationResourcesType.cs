﻿using System;

namespace Infrastructure.Validation
{
    public class OverridenLocalizationResourcesType
    {
        public OverridenLocalizationResourcesType(Type value)
        {
            Value = value;
        }

        public Type Value { get; }
    }
}