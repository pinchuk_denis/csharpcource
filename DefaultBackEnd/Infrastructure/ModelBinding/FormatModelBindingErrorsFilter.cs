﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using Infrastructure.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Infrastructure.ModelBinding
{
    public class FormatModelBindingErrorsFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (context.ModelState.IsValid)
                await next();
            else
                CreateModelBindingError(context);
        }

        private static void CreateModelBindingError(ActionExecutingContext context)
        {
            var validationFailures = new List<ValidationFailure>();
            foreach (var property in context.ModelState)
            foreach (var propertyBindError in property.Value.Errors)
            {
                var errorMessage = GetPropertyErrorMessage(propertyBindError);

                var propertyKeyInPascalNotation = GetPropertyKeyInPascalCase(property.Key);

                var validationFailure = new ValidationFailure(propertyKeyInPascalNotation,
                    errorMessage.Replace(property.Key, propertyKeyInPascalNotation))
                {
                    ErrorCode = "Error_" + propertyKeyInPascalNotation.Replace(".", "_") + "_IncorrectValue"
                };

                validationFailures.Add(validationFailure);
            }

            throw new ModelBindingException(validationFailures);
        }

        private static string GetPropertyErrorMessage(ModelError propertyBindError)
        {
            if (string.IsNullOrWhiteSpace(propertyBindError.ErrorMessage)) return propertyBindError.Exception.Message;

            return propertyBindError.ErrorMessage;
        }

        private static string GetPropertyKeyInPascalCase(string propertyKey)
        {
            if (string.IsNullOrWhiteSpace(propertyKey)) return string.Empty;

            var namespaceAreas = propertyKey.Split(".");

            return string.Join('.', namespaceAreas.Select(x => char.ToUpperInvariant(x[0]) + x.Substring(1)));
            ;
        }
    }
}