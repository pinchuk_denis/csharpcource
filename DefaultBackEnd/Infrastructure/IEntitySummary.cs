﻿namespace Infrastructure
{
    public interface IEntitySummary<TId> : IEntityKey<TId>
    {
        string Name { get; set; }
    }
}